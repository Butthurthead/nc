package ru.ncedu.ivanov.shashki.ui;

import java.io.PrintStream;
import java.util.List;

import ru.ncedu.ivanov.shashki.game.FigureInfo;
import ru.ncedu.ivanov.shashki.game.GameState;

public class ShashkiConsoleView implements ShashkiView {
	private static final int CELL_SIZE = 4;
	
	private PrintStream out = System.out;
	
	public synchronized void showMessage(String message) {
		out.println(message);
	}
	
	private void figuresToArray(List<FigureInfo> figures, int prior, String[][] output) {
		for (FigureInfo f : figures) {
			output[f.position.y][f.position.x] = ((prior == 0) ? "A" : "B") + f.id + ((f.damka) ? "d" : "");
		}
	}
	
	private String stringInCell(String str) {
		int lenDiff = CELL_SIZE - str.length();
		String res = str;
		if (lenDiff < 0) {
			res = str.substring(0, CELL_SIZE - 1);
		}
		else if (lenDiff > 0) {
			for (int i = 0; i < lenDiff; i++) res += " ";
		}
		return res;
	}
	
	private void printLine() {
		for (int j = 0; j < ((GameState.BOARD_SIZE + 1) * (CELL_SIZE + 1)); j++) out.print("=");
		out.println("");
	}
	
	private void printEmptyRow() {
		for (int i = 0; i < GameState.BOARD_SIZE + 1; i++) out.print(stringInCell("") + "|");
		out.println("");
	}
	
	public synchronized void showGameState(GameState state) {
		String[][] cells = new String[GameState.BOARD_SIZE][GameState.BOARD_SIZE];
		figuresToArray(state.getPlayers()[0].getFigures(), 0, cells);
		figuresToArray(state.getPlayers()[1].getFigures(), 1, cells);
		
		out.print(stringInCell("") + "|");
		for (int i = 0; i < GameState.BOARD_SIZE; i++) out.print(stringInCell("" + i) + "|");
		out.println("");
		printLine();
		
		for (int i = 0; i < GameState.BOARD_SIZE; i++) {
			out.print(stringInCell(i + "") + "|");
			for (int j = 0; j < GameState.BOARD_SIZE; j++) {
				out.print(stringInCell( (cells[i][j] != null) ? cells[i][j] : "" ) + "|");
			}
			out.println("");
			for (int j = 0; j < (CELL_SIZE - 1); j++) printEmptyRow();
			printLine();
		}
	}
}
