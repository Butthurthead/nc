package ru.ncedu.ivanov.shashki.ui;

import ru.ncedu.ivanov.shashki.game.GameState;

public interface ShashkiView {
	
	/**
	 * Shows text message to user.
	 * @param message
	 */
	public void showMessage(String message);
	
	/**
	 * Shows information about alive figures and their location on screen.
	 * @param state
	 */
	public void showGameState(GameState state);
	
}
