package ru.ncedu.ivanov.shashki.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.ncedu.ivanov.shashki.game.Cell;
import ru.ncedu.ivanov.shashki.game.GameController;
import ru.ncedu.ivanov.shashki.game.TurnInfo;
import ru.ncedu.ivanov.shashki.managers.ConfigManager;

public class ConsoleController {
	private ShashkiView view;
	private GameController gameController;
	
	private static final String CONFIG_FILE_PATHNAME = "C:/Users/Alexander/eclipse_workspace/shashki/config.xml";
	
	public ConsoleController() {
		view = new ShashkiConsoleView();
		gameController = null;
	}
	
	private TurnInfo parseTurnInfo(String input, int playerPriority) {
		Pattern p = Pattern.compile("^(\\d+)\\s+(.*)$");
		Matcher m = p.matcher(input);
		if (!m.matches()) {
			return null;
		}
		int figId;
		try {
			figId = Integer.parseInt(m.group(1));
		}
		catch (NumberFormatException nf) {
			return null;
		}
		input = m.group(2);
		Pattern p1 = Pattern.compile("\\((\\d+),\\s*(\\d+)\\)");
		Matcher m1 = p1.matcher(input);
		LinkedList<Cell> path = new LinkedList<Cell>();
		try {
			while (m1.find()) {
				String s1 = m1.group(1);
				int x = Integer.parseInt(s1);
				String s2 = m1.group(2);
				int y = Integer.parseInt(s2);
				Cell c = new Cell(x, y);
				path.add(c);
			}
		}
		catch (NumberFormatException nf) {
			return null;
		}
		TurnInfo turn = new TurnInfo(figId, playerPriority, path);
		return turn;
	}
	
	private void createNewGame(ConfigManager.ConfigParam configuration) {
		try {
			gameController = GameController.createGame(configuration, view);
			gameController.start();
		}
		catch (Exception e) {
			view.showMessage("Failed to create game: " + e.getMessage());
			gameController = null;
			return;
		}
	}
	
	private void joinExistingGame(ConfigManager.ConfigParam configuration, String host) {
		try {
			gameController = GameController.joinGame(configuration, host, view);
			gameController.start();
		}
		catch (Exception e) {
			view.showMessage("Failed to join game: " + e.getMessage());
			gameController = null;
			return;
		}
	}
	
	private void startInput() {
		if (gameController == null) {
			return;
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while (!gameController.isOver()) {
			try {
				if (!gameController.readyForInput()) {
					continue;
				}
				String input = br.readLine();
				TurnInfo turn = parseTurnInfo(input, gameController.getMyPriority());
				if (turn == null) {
					view.showMessage("Incorrect input");
					continue;
				}
				gameController.setTurn(turn);
			}
			catch (IOException io) {
				view.showMessage("Input failed");
				gameController.endGame();
				return;
			}
		}
	}
	
	public void startGame(String[] args) {
		if ((args == null) || (args.length < 1)) {
			view.showMessage("No arguments set");
			return;
		}
		ConfigManager.ConfigParam param = null;
		try {
			param = new ConfigManager(CONFIG_FILE_PATHNAME).getParameters();
		}
		catch (Exception e) {
			view.showMessage("Failed to load configuration: " + e.getMessage());
			return;
		}
		if (args[0].equals("create")) {
			createNewGame(param);
		}
		else if (args[0].equals("join")) {
			if (args.length < 2) {
				view.showMessage("No host set");
				return;
			}
			String host = args[1];
			joinExistingGame(param, host);
		}
		else {
			view.showMessage("Incorrect arguments");
			return;
		}
		startInput();
	}
}
