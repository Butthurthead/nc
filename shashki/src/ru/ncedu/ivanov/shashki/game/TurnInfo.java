package ru.ncedu.ivanov.shashki.game;

import java.util.LinkedList;
import java.util.List;
import java.io.Serializable;

public class TurnInfo implements Serializable {
	private List<Cell> path;
	private int figureId;
	private int playerPriority;
	
	private static final long serialVersionUID = 102L;
	
	public TurnInfo(int figureId, int playerPriority) {
		this(figureId, playerPriority, new LinkedList<Cell>());
	}
	
	public TurnInfo(int figureId, int playerPriority, LinkedList<Cell> path) {
		this.figureId = figureId;
		this.playerPriority = playerPriority;
		this.path = path;
	}
	
	public TurnInfo(int figureId, int playerPriority, Cell cell) {
		this(figureId, playerPriority);
		path = new LinkedList<Cell>();
		path.add(cell);
	}
	
	public List<Cell> getPath() {
		return path;
	}
	
	public int getPlayerPriority() {
		return playerPriority;
	}
	
	public int getFigureId() {
		return figureId;
	}
	
	public String toString() {
		String s = figureId+" " + playerPriority + " [";
		for (Cell c : path) {
			s += c+", ";
		}
		s += "]";
		return s;
	}
}
