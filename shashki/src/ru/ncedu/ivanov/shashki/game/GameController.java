package ru.ncedu.ivanov.shashki.game;

import ru.ncedu.ivanov.shashki.managers.ConfigManager;
import ru.ncedu.ivanov.shashki.managers.DBManager;
import ru.ncedu.ivanov.shashki.managers.MessageManager;
import ru.ncedu.ivanov.shashki.ui.ShashkiView;

import java.io.*;
import java.sql.SQLException;
import java.util.Date;

public class GameController {
	private GameState state;
	private DBManager dbManager;
	private MessageManager msgManager;
	private TurnInfo currentTurn = null;
	private Date startDate;
	private ShashkiView view;
	
	private GameController(ConfigManager.ConfigParam parameters) throws Exception {
		state = new GameState();
		dbManager = new DBManager(parameters.getTableName());
		msgManager = new MessageManager(parameters.getGamePort());
		startDate = new Date();
	}
	
	/**
	 * Creates new game according to the given parameters and returns Game instance
	 * after another player joins this game and players send their nicknames to each other.
	 * @param parameters - configuration parameters
	 * @param out - printStream instance to output errors and messages
	 * @return Game instance
	 * @throws Exception - when unexpected message is received from another player,
	 * connection is aborted or output failed
	 */
	public static GameController createGame(ConfigManager.ConfigParam parameters, ShashkiView view) throws Exception {
		GameController game = new GameController(parameters);
		game.view = view;
		
		game.view.showMessage("Listening port "+parameters.getGamePort()+"...");
		
		game.msgManager.listen();
		String opponentName = (String)game.msgManager.recvMsg();
		game.msgManager.sendMsg(parameters.getUserName());
		game.state.setPlayerName(0, parameters.getUserName());
		game.state.setPlayerName(1, opponentName);
		game.state.setMyPriority(0);
		
		game.view.showMessage(opponentName + " connected");
		
		return game;
	}
	
	/**
	 * Joins game on given host according to the given parameters and returns new Game instance
	 * after the connection is established and players send their nicknames to each other.
	 * @param parameters - configuration parameters
	 * @param host - host to connect
	 * @param out - printStream instance to output errors and messages
	 * @return Game instance
	 * @throws Exception - when unexpected message is received from another player,
	 * connection is aborted or output failed
	 */
	public static GameController joinGame(ConfigManager.ConfigParam parameters, String host, ShashkiView view) throws Exception {
		GameController game = new GameController(parameters);
		game.view = view;
		
		game.view.showMessage("Connecting to "+host+":"+parameters.getGamePort());
		
		game.msgManager.connect(host);
		game.msgManager.sendMsg(parameters.getUserName());
		String opponentName = (String)game.msgManager.recvMsg();
		game.state.setPlayerName(0, opponentName);
		game.state.setPlayerName(1, parameters.getUserName());
		game.state.setMyPriority(1);
		
		game.view.showMessage("Connected to " + opponentName + "'s game");
		
		return game;
	}
	
	/**
	 * Return user's priority in making turns as an integer. 0 - user is first to make turn, 1 - second, etc.
	 * @return user's priority
	 */
	public int getMyPriority() {
		return state.getMyPriority();
	}
	
	/**
	 * Starts processing players' input and outputting messages and errors in new thread.
	 */
	public void start() {
		Thread t = new Thread(new GameRunnable());
		t.start();
	}
	
	/**
	 * Returns true if now it's user's turn and false if it's opponents turn
	 * @return true or false
	 */
	public synchronized boolean readyForInput() {
		return ((currentTurn == null) && (state.getGameStatus() == GameStatus.PLAYING));
	}
	
	/**
	 * Returns true if one of players won or if game ended because of an error.
	 * @return true or false
	 */
	public synchronized boolean isOver() {
		return ((state.getGameStatus() != GameStatus.WAITING) && (state.getGameStatus() != GameStatus.PLAYING));
	}
	
	/**
	 * Exits game, saves game result in DB.
	 */
	public synchronized void endGame() {
		try {
			msgManager.closeConn();
		}
		catch (IOException io) {
			view.showMessage("Failed to close connection: " + io);
		}
		if (!dbManager.resultsSaved()) {
			try {
				dbManager.saveResults(state.getPlayerName(0), state.getPlayerName(1),
						startDate, new Date(), state.getGameStatus().toString());
			}
			catch (SQLException sqlex) {
				view.showMessage("Failed to save results in DB: " + sqlex.getMessage());
			}
		}
		String message = null;
		switch (state.getGameStatus()) {
		case A_WIN:
			message = state.getPlayerName(0) + "wins";
			break;
		case B_WIN:
			message = state.getPlayerName(1) + "wins";
			break;
		default:
			message = "Game stopped because of error";
		}
		view.showMessage(message);
	}
	
	/**
	 * Set user's turn, that will be processed in closest iteration of turns processing thread.
	 * @param turn - a TurnInfo instance represents turn to make
	 */
	public synchronized void setTurn (TurnInfo turn) {
		if ((state.getGameStatus() == GameStatus.PLAYING) && (currentTurn == null)) {
			currentTurn = turn;
		}
	}
	
	private synchronized void usrTurn() throws Exception {
		if (currentTurn == null) {
			return;
		}
		if (state.applyTurn(currentTurn)) {
			msgManager.sendMsg(currentTurn);
			view.showGameState(state);
		}
		else {
			view.showMessage("Illegal turn: " + state.getError());
		}
		currentTurn = null;
	}
	
	private synchronized void opnTurn() throws Exception {
		TurnInfo turn = (TurnInfo)msgManager.recvMsg();
		if (state.applyTurn(turn)) {
			view.showGameState(state);
		}
		else {
			view.showMessage("Unexpected data is recieved, close connection");
			state.fail();
		}
	}
	
	class GameRunnable implements Runnable {
		
		@Override
		public void run() {
			view.showMessage("Game started");
			view.showGameState(state);
			try {
				boolean notEnded = true;
				while (notEnded) {
					switch (state.getGameStatus()) {
					case WAITING:
						opnTurn();
						break;
					case PLAYING:
						usrTurn();
						break;
					default:
						notEnded = false;
					}
				}
			}
			catch (Exception e) {
				state.fail();
				view.showMessage("Data exchange error: " + e.getMessage());
			}
			finally {
				endGame();
			}
		}
		
	}

}
