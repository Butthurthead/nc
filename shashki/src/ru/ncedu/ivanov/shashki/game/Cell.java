package ru.ncedu.ivanov.shashki.game;

import java.io.Serializable;

public class Cell implements Serializable {
	private static final long serialVersionUID = 101L;

	public int x;
	public int y;	
	
	public Cell(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean equals(Cell c) {
		return (x == c.x) && (y == c.y);
	}
	
	public String toString() {
		return "("+x+";"+y+")";
	}
}
