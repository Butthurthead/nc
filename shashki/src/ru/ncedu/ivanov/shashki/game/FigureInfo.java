package ru.ncedu.ivanov.shashki.game;

public class FigureInfo {
	public Cell position;
	public boolean damka;
	public boolean alive;
	public int id;
	
	public FigureInfo(Cell position, boolean damka, boolean alive, int id) {
		this.position = position;
		this.damka = damka;
		this.alive = alive;
		this.id = id;
	}
	
	public Object clone() {
		return new FigureInfo(new Cell(this.position.x, this.position.y), this.damka, this.alive, this.id);
	}
}
