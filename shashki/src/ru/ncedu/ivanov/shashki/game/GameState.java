package ru.ncedu.ivanov.shashki.game;

import java.util.List;
import java.util.ListIterator;
import java.util.ArrayList;

public class GameState {
	private Player[] players;
	private GameStatus gameStatus;
	private int myPriority;
	private String error;
	
	public static final int BOARD_SIZE = 8;
	
	private GameState(boolean initFigures) {
		gameStatus = null;
		error = "";
		List<FigureInfo> aFigures = new ArrayList<FigureInfo>();
		List<FigureInfo> bFigures = new ArrayList<FigureInfo>();
		if (initFigures) {
			int id = 0;
			for (int i=0; i<3; i++) {
				for (int j = (i%2); j < BOARD_SIZE; j += 2) {
					Cell a = new Cell(j,i);
					aFigures.add(new FigureInfo(a,false,true,id));
					Cell b = new Cell(7-j,7-i);
					bFigures.add(new FigureInfo(b,false,true,id));
					++id;
				}
			}
		}
		players = new Player[] {new Player(aFigures, null), new Player(bFigures, null)};
	}
	
	/**
	 * Creates new GameState instance with two players with 12 figures in initial positions.
	 */
	public GameState() {
		this(true);
	}
	
	/**
	 * Creates new GameState instance with given figures location and quantity.
	 * @param aFigures - a list of FigureInfo instances representing figures' location for player A (first to make turn)
	 * @param bFigures - the same for player B (second to make turn)
	 */
	public GameState(List<FigureInfo> aFigures, List<FigureInfo> bFigures) {
		players = new Player[] {new Player(aFigures, null), new Player(bFigures, null)};
		gameStatus = null;
		error = "";
	}
	
	/**
	 * Returns player's name by given player's priority.
	 * @param priority - an integer represents player's priority to make turn
	 * @return a name of player with given priority
	 */
	public String getPlayerName(int priority) {
		return players[priority].getName();
	}
	
	/**
	 * Sets player's name by given player's priority.
	 * @param priority - an integer represents player's priority to make turn
	 * @param newName - a name to set for player
	 */
	public void setPlayerName(int priority, String newName) {
		players[priority].setName(newName);;
	}
	
	/**
	 * Returns priority of making a turn for user.
	 * @return priority as an integer
	 */
	public int getMyPriority() {
		return myPriority;
	}
	
	/**
	 * Sets priority for user.
	 * @param priority - an integer represents player's priority to make turn
	 * @throws IllegalArgumentException if priority value is illegal, for example, 2 when there are only 
	 * two players or less than 0
	 */
	public void setMyPriority(int priority) throws IllegalArgumentException {
		myPriority = priority;
		if (priority == 0) {
			gameStatus = GameStatus.PLAYING;
		} else if (priority == 1) {
			gameStatus = GameStatus.WAITING;
		}
		else {
			throw new IllegalArgumentException("illegal user priority was set");
		}
	}
	
	public Player[] getPlayers() {
		return players;
	}
	
	/**
	 * Returns GameStatus representing current status of game.
	 * WAITING - user waits for opponent to make turn
	 * PLAYING - it's user's turn
	 * A_WIN - first user to make turn wins
	 * B_WIN - second user to make turn wins
	 * FAILED - game finished because of an error
	 * @return GameStatus
	 */
	public GameStatus getGameStatus() {
		return gameStatus;
	}
	
	/**
	 * Returns message for user from his last attempt to make turn. Message occurs when
	 * user makes incorrect input or tries not to play by the rules.
	 * @return String instance with message
	 */
	public String getError() {
		return error;
	}
	
	/**
	 * Makes game status FAILED. It's useful to stop users' input
	 * processing thread in Game class when exception is thrown.
	 */
	public void fail() {
		gameStatus = GameStatus.FAILED;
	}
	
	public Object clone() {
		GameState gameStateCopy = new GameState(false);
		gameStateCopy.gameStatus = gameStatus;
		gameStateCopy.myPriority = myPriority;
		gameStateCopy.players[0].setFigures(players[0].cloneFigures());
		gameStateCopy.players[1].setFigures(players[1].cloneFigures());
		return gameStateCopy;
	}
	
	private boolean outOfBoard(Cell cell) {
		return ((cell.x < 0) || (cell.x >= BOARD_SIZE) || (cell.y < 0) || (cell.y >= BOARD_SIZE));
	}
	
	private boolean cellIsBusy(Cell cell) {
		if ((players[0].busy(cell) == null) && (players[1].busy(cell) == null)) {
			return false;
		}
		return true;
	}
	
	public class Player {
		private List<FigureInfo> figures;
		private String name;
		
		public Player(List<FigureInfo> figures, String name) {
			this.figures = figures;
			this.name = name;
		}
		
		public Player() {
			this(new ArrayList<FigureInfo>(), null);
		}
		
		public List<FigureInfo> getFigures() {
			return figures;
		}
		
		public void setFigures(List<FigureInfo> figures) {
			this.figures = figures;
		}
		
		public String getName() {
			return name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
		public List<FigureInfo> cloneFigures() {
			List<FigureInfo> copy = new ArrayList<FigureInfo>();
			for (FigureInfo f : figures) {
				copy.add((FigureInfo)f.clone());
			}
			return copy;
		}
		
		public FigureInfo busy(Cell cell) {
			for (FigureInfo f : figures) {
				if ((f.alive) && (f.position.equals(cell))) {
					return f;
				}
			}
			return null;
		}
		
		private int[][] getDirections(int figureId) {
			FigureInfo figure = figures.get(figureId);
			if (figure.damka) {
				int[][] arr = {{1, 1}, {-1, 1}, {-1, -1}, {1, -1}};
				return arr;
			}
			else {
				if (players[0] == this) {
					int[][] arr = {{1, 1}, {-1, 1}};
					return arr;
				}
				else if (players[1] == this) {
					int[][] arr = {{1, -1}, {-1, -1}};
					return arr;
				}
				else {
					return null;
				}
			}
		}
		
		public boolean canMove(int figureId) {
			FigureInfo figure = figures.get(figureId);
			if (!figure.alive) {
				return false;
			}
			int[][] directions = getDirections(figure.id);
			int maxSteps = (figure.damka) ? (BOARD_SIZE - 1) : 2;
			for (int[] d : directions) {
				int oppFigures = 0;
				for (int i = 1; i <= maxSteps; i++) {
					Cell moveTo = new Cell(figure.position.x + d[0] * i, figure.position.y + d[1] * i);
					if ((outOfBoard(moveTo)) || (this.busy(moveTo) != null) || (oppFigures >= 2)) {
						break;
					}
					if (!cellIsBusy(moveTo)) {
						return true;
					}
					++oppFigures;
				}
			}
			return false;
		}
		
		public boolean canHit(int figureId) {
			FigureInfo figure = figures.get(figureId);
			if (!figure.alive) {
				return false;
			}
			Player opponent = (players[0] == this) ? players[1] : players[0];
			int[][] directions = getDirections(figure.id);
			int maxSteps = (figure.damka) ? (BOARD_SIZE - 1) : 2;
			for (int[] d : directions) {
				for (int k = 2; k <= maxSteps; k++) {
					Cell moveTo = new Cell(figure.position.x + k * d[0], figure.position.y + k*d[1]);
					if (outOfBoard(moveTo)) {
						break;
					}
					Cell moveOver = new Cell(figure.position.x + (k - 1) * d[0], figure.position.y + (k-1)*d[1]);
					if (this.busy(moveOver) != null) {
						break;
					}
					if ( (opponent.busy(moveOver) != null) && (!cellIsBusy(moveTo)) ) {
						return true;
					}
				}
			}
			return false;
		}
		
		public boolean canHit() {
			for (FigureInfo f : figures) {
				if (canHit(f.id)) {
					return true;
				}
			}
			return false;
		}
	}
	
	private int moveFigure(int playerPriority, int figureId, Cell pos) { //returns number of figures hit or -1 if movement is illegal
		Player player = players[playerPriority];
		Player opponent = (playerPriority == 0) ? players[1] : players[0];
		FigureInfo figure = players[playerPriority].getFigures().get(figureId);
		int dx = pos.x - figure.position.x;
		int dy = pos.y - figure.position.y;
		
		if ((dx == 0) || (Math.abs(dy) != Math.abs(dx)) ) {
			error = "only diagonal movements are allowed";
			return -1;
		}
		if ((!figure.damka) && (Math.abs(dx) > 2)) {
			error = "only 1 or 2 cell movements are allowed for normal figures";
			return -1;
		}
		if ( (!figure.damka) && ( ((playerPriority == 0) && (dy < 0)) || ((playerPriority == 1) && (dy > 0)) ) ) {
			error = "only forward movements are allowed";
			return -1;
		}
		if ( (outOfBoard(pos)) || (cellIsBusy(pos)) ) {
			error = "you can't move figure to a busy postion or out of board";
			return -1;
		}
		int[] step = {((dx >= 0) ? 1 : -1), ((dy >= 0) ? 1 : -1)};
		Cell curCell = new Cell(figure.position.x+step[0], figure.position.y+step[1]);
		int hitFigures = 0;
		
		if (!figure.damka) {
			if (curCell.equals(pos)) {
				hitFigures = 0;
			}
			else {
				FigureInfo oppFigure = opponent.busy(curCell);
				if (oppFigure == null) {
					error = "only 1 cell movements are allowed for normal figures if no figure was hit";
					return -1;
				}
				oppFigure.alive = false;
				hitFigures = 1;
			}
		}
		else {
			while (!curCell.equals(pos)) {
				FigureInfo oppFigure = opponent.busy(curCell);
				if (oppFigure != null) {
					Cell nextCell = new Cell(curCell.x + step[0], curCell.y + step[0]);
					if ((player.busy(nextCell) != null) || (opponent.busy(nextCell) != null)) {
						error = "the position after beaten figure is busy";
						return -1;
					}
					oppFigure.alive = false;
					++hitFigures;
				}
				else if (player.busy(curCell) != null) {
					error = "you can't step over your figures";
					return -1;
				}
				curCell.x += step[0];
				curCell.y += step[1];
			}
		}
		
		figure.position = pos;
		if ( ((playerPriority == 0) && (pos.y == BOARD_SIZE - 1)) || ((playerPriority == 1) && (pos.y == 0)) ) {
			figure.damka = true;
		}
		return hitFigures;
	}
	
	/**
	 * Applies player's turn. Returns true if turn is legal and successfully applied and false
	 * if game rules are violated or input is incorrect. Information about
	 * reasons why this method returns false can be get by getError() method.
	 * @param turn - TurnInfo instance representing player's turn
	 * @return true or false
	 */
	public boolean applyTurn(TurnInfo turn) {
		if (turn == null) {
			return false;
		}
		if ((turn.getPath() == null) || (turn.getPath().size() < 1) ||
			(turn.getPlayerPriority() < 0) || (turn.getPlayerPriority() > 1)) {
			error = "incorrect input format";
			return false;
		}
		if ( ((myPriority == turn.getPlayerPriority()) && (gameStatus != GameStatus.PLAYING)) ||
			 ((myPriority != turn.getPlayerPriority()) && (gameStatus != GameStatus.WAITING))	) {
			error = "turn priority violated";
			return false;
		}
		FigureInfo figure = null;
		try {
			figure = players[turn.getPlayerPriority()].getFigures().get(turn.getFigureId());
		}
		catch (IndexOutOfBoundsException iob) {
			error = "no such figure";
			return false;
		}
		if (!figure.alive) {
			error = "this figure is out of game";
			return false;
		}
		GameState tempState = (GameState)clone(); //create a copy to discard changes if turn will be illegal
		ListIterator<Cell> iter = turn.getPath().listIterator();
		int hitFigures;
		do {
			boolean firstStep = !(iter.hasPrevious());
			Cell cur = iter.next();
			boolean canHit = false;
			if (cur == turn.getPath().get(0)) {
				canHit = tempState.players[turn.getPlayerPriority()].canHit();
			}
			else {
				canHit = tempState.players[turn.getPlayerPriority()].canHit(figure.id);
			}
			hitFigures = tempState.moveFigure(turn.getPlayerPriority(), figure.id, cur);
			if (hitFigures < 0) { //current step is illegal => illegal turn
				error = tempState.getError();
				return false;
			}
			else if(hitFigures == 0) {
				if (canHit) {
					error = "you must hit a figure if you can";
					return false;
				}
				if (iter.hasNext()) {
					error = "you can't move figure if no figures were hit in previous step";
					return false;
				}
				if (!firstStep) {
					error = "you can do more than one step only if you hit a figure by this step";
					return false;
				}
			}
		} while (iter.hasNext());
		if ((hitFigures > 0) && (tempState.players[turn.getPlayerPriority()].canHit(figure.id))) {
			error = "you must hit as many figures as you can";
			return false;
		}
		//apply changes
		players = tempState.players;
		int alive = 0;
		int canMove = 0;
		Player opponent = (turn.getPlayerPriority() == 0) ? players[1] : players[0];
		for (FigureInfo f :  opponent.getFigures()) {
			if (f.alive) {
				++alive;
			}
			if (opponent.canMove(f.id)) {
				++canMove;
			}
		}
		//if all opponent's figures are dead or locked - player who made a turn wins
		if ((canMove == 0) || (alive == 0)) {
			gameStatus = (turn.getPlayerPriority() == 0) ? GameStatus.A_WIN : GameStatus.B_WIN;
		}
		else {
			gameStatus = (turn.getPlayerPriority() == myPriority) ? GameStatus.WAITING : GameStatus.PLAYING;
		}
		return true;
	}
	
}
