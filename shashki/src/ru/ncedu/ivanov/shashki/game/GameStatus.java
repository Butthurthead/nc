package ru.ncedu.ivanov.shashki.game;

public enum GameStatus {
	WAITING, PLAYING, A_WIN, B_WIN, FAILED;
}
