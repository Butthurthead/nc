package ru.ncedu.ivanov.shashki;

import ru.ncedu.ivanov.shashki.ui.ConsoleController;

public class Main {

	public static void main(String[] args) {
		ConsoleController sc = new ConsoleController();
		sc.startGame(args);
	}

}
