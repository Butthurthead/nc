package ru.ncedu.ivanov.shashki.test;

import ru.ncedu.ivanov.shashki.game.*;
import java.util.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.runners.Parameterized.*;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class GameStateTest {
	private GameState state = null;
	private int[][] aPositions;
	private int[] aDamkas;
	private int[][] bPositions;
	private int[] bDamkas;
    private int figureId;
    private int[][] path;
    private Boolean isLegal;
    private GameStatus expectedStatus;
	
	private static TurnInfo createTurn(int figureId, int playerPriority, int[][] points) {
		LinkedList<Cell> path = new LinkedList<Cell>();
		for (int[] p : points) {
			path.add(new Cell(p[0], p[1]));
		}
		return new TurnInfo(figureId, playerPriority, path);
	}
	
	public GameStateTest(int[][] aPositions, int[] aDamkas,
			             int[][] bPositions, int[] bDamkas,
                         int figureId, int[][] path, Boolean isLegal,
                         GameStatus expectedStatus) {
		this.aPositions = aPositions;
		this.aDamkas = aDamkas;
		this.bPositions = bPositions;
		this.bDamkas = bDamkas;
		this.figureId = figureId;
		this.path = path;
		this.isLegal = isLegal;
		this.expectedStatus = expectedStatus;
	}
	
	@Before
	public void initGameState() {
		List<FigureInfo> aFigures = new ArrayList<FigureInfo>();
		int id = 0;
		for (int[] p : aPositions) {
			aFigures.add(new FigureInfo(new Cell(p[0], p[1]), false, true, id));
			++id;
		}
		for (int i=id; i<8; i++) {
			aFigures.add(new FigureInfo(new Cell(0, 0), false, false, id));
		}
		for (int i : aDamkas) {
			aFigures.get(i).damka = true;
		}
		List<FigureInfo> bFigures = new ArrayList<FigureInfo>();
		id = 0;
		for (int[] p : bPositions) {
			bFigures.add(new FigureInfo(new Cell(p[0], p[1]), false, true, id));
			++id;
		}
		for (int i=id; i<8; i++) {
			bFigures.add(new FigureInfo(new Cell(0, 0), false, false, id));
		}
		for (int i : bDamkas) {
			bFigures.get(i).damka = true;
		}
		state = new GameState(aFigures, bFigures);
		state.setMyPriority(0);
	}
	
	@Test
	public void runTurn() {
		Boolean result = new Boolean(state.applyTurn(createTurn(figureId, 0, path)));
		assertEquals("Incorrect turn verification", isLegal, result);
		if (expectedStatus != null) {
			assertEquals("Incorrect game status after making turn", expectedStatus, state.getGameStatus());
		}
	}
	
	@Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
        	{ new int[][] { {5,0} }, new int[] {},
			  new int[][] { {2,3}, {4,1} }, new int[]{},
			  0, new int[][] {{3,2}, {1,4}}, new Boolean(true), GameStatus.A_WIN },
        	
        	{ new int[][] { {0,0} }, new int[] {},
			  new int[][] { {1,1}, {1,3}, {1,5} }, new int[] {},
			  0, new int[][] { {2,2}, {0,4}, {2,6} }, new Boolean(true), GameStatus.A_WIN },
        	
        	{ new int[][] { {0,0} }, new int[] {0},
			  new int[][] { {2,2}, {4,4} }, new int[] {},
			  0, new int[][] { {3,3}, {6,6} }, new Boolean(true), GameStatus.A_WIN },
        	
        	{ new int[][] { {0,0} }, new int[] {0},
			  new int[][] { {2,2}, {1,5} }, new int[] {},
			  0, new int[][] { {3,3}, {0,6} }, new Boolean(true), GameStatus.A_WIN },
        	
        	{ new int[][] { {6,7} }, new int[] {0},
			  new int[][] { {1,2}, {3,4}, {5,6} }, new int[] {},
			  0, new int[][] { {0,1} }, new Boolean(true), GameStatus.A_WIN },
        	
        	{ new int[][] { {0,0}, {4,4} }, new int[] {0},
			  new int[][] { {2,2}, {5,5} }, new int[] {},
			  0, new int[][] { {3,3} }, new Boolean(true), GameStatus.WAITING },
        	
        	{ new int[][] { {0,0} }, new int[] {0},
			  new int[][] { {2,2}, {4,4}, {7,7} }, new int[] {},
			  0, new int[][] { {5,5} }, new Boolean(true), GameStatus.WAITING },
        	
        	{ new int[][] { {1,1} }, new int[] {},
			  new int[][] { {2,2}, {2,4}, {2,6} }, new int[] {},
			  0, new int[][] { {3,3}, {1,5}, {3,7} }, new Boolean(true), GameStatus.A_WIN },
        	
        	{ new int[][] { {2,0}, {2,2} }, new int[] {},
			  new int[][] { {0,0} }, new int[] {},
			  0, new int[][] { {1,1} }, new Boolean(true), GameStatus.A_WIN },
        	
        	{ new int[][] { {0,0} }, new int[] {},
			  new int[][] { {1,1} }, new int[] {},
			  0, new int[][] { {2,2} }, new Boolean(true), GameStatus.A_WIN },
        	
        	{ new int[][] { {7,5} }, new int[] {},
			  new int[][] { {7,7} }, new int[] {},
			  0, new int[][] { {6,6} }, new Boolean(true), GameStatus.WAITING },
        	
        	{ new int[][] { {7,5}, {5,5} }, new int[] {},
			  new int[][] { {7,7} }, new int[] {},
			  0, new int[][] { {6,6} }, new Boolean(true), GameStatus.A_WIN },
        	
        	{ new int[][] { {7,5} }, new int[] {},
			  new int[][] { {6,6}, {4,6} }, new int[] {},
			  0, new int[][] { {5,7}, {2,4} }, new Boolean(true), GameStatus.A_WIN }
           });
    }
	
}
