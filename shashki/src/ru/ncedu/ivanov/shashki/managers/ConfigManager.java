package ru.ncedu.ivanov.shashki.managers;

import org.w3c.dom.*;

import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.*;
import java.io.*;

public class ConfigManager {
	private final String documentFileName;
	private final String schemaFileName = "C:/Users/Alexander/eclipse_workspace/shashki/schema.xsd";
	
	public ConfigManager(String documentFileName) {
		this.documentFileName = documentFileName;
	}
	
	public ConfigParam getParameters() throws Exception {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = builder.parse(new File(documentFileName));
		
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = factory.newSchema(new File(schemaFileName));
		
		Validator validator = schema.newValidator();
		validator.validate(new DOMSource(document));
		
		Element root = document.getDocumentElement();
		String userName = root.getElementsByTagName("user-name").item(0).getTextContent();
		int gamePort = Integer.parseInt(root.getElementsByTagName("game-port").item(0).getTextContent());
		String tableName = root.getElementsByTagName("table-name").item(0).getTextContent();
		
		return new ConfigParam(userName, gamePort, tableName);
	}
	
	public class ConfigParam {
		private final String userName;
		private final int gamePort;
		private final String tableName;
		
		public ConfigParam(String userName, int gamePort, String tableName) {
			this.userName = userName;
			this.gamePort = gamePort;
			this.tableName = tableName;
		}
		
		public String getUserName() {
			return userName;
		}
		
		public int getGamePort() {
			return gamePort;
		}
		
		public String getTableName() {
			return tableName;
		}
		
	}
}
