package ru.ncedu.ivanov.shashki.managers;

import java.util.Date;
import java.sql.*;

public class DBManager {
	private final String table_name;
	private boolean results_saved;
	
	public DBManager(String table_name) {
		this.table_name = table_name;
		this.results_saved = false;
	}
	
	public void saveResults(String a_name, String b_name, Date started, Date ended, String result)
	throws SQLException {
		Connection c = DriverManager.getConnection("jdbc:sqlite:shashki.db");
		
		Statement createTable = c.createStatement();
		String createTableStr = "CREATE TABLE IF NOT EXISTS "+table_name+
				          " (id INTEGER PRIMARY KEY AUTOINCREMENT, "+
		                  "player_a_name TEXT, " + 
				          "player_b_name TEXT, " +
		                  "started TEXT, " +
				          "ended TEXT, " +
		                  "result CHAR(20) )";
		createTable.execute(createTableStr);
		createTable.close();
		
		Statement insertResult = c.createStatement();
		String insertResultStr = "INSERT INTO "+table_name+"(player_a_name, player_b_name, started, ended, result)"+
		                    " VALUES( '"+a_name+"' , '"+b_name+"' "+
				                    ", '"+started.toString()+"' , '"+ended.toString()+"' , '"+result+"' )";
		insertResult.executeUpdate(insertResultStr);
		insertResult.close();
		
		c.close();
		results_saved = true;
	}
	
	public boolean resultsSaved() {
		return results_saved;
	}
	
}
