package ru.ncedu.ivanov.shashki.managers;

import java.net.*;
import java.nio.channels.IllegalBlockingModeException;
import java.io.*;

public class MessageManager {
	private final int port;
	private Socket sock = null;
	private ObjectOutputStream objectOutputStream = null;
	private ObjectInputStream objectInputStream = null;
	private boolean closed;
	
	public MessageManager(int port) {
		this.port = port;
		this.closed = false;
	}
	
	public void listen() throws IOException, SecurityException, IllegalArgumentException,
	SocketTimeoutException, IllegalBlockingModeException {
		ServerSocket ss = new ServerSocket(port);
		sock = ss.accept();
		objectOutputStream = new ObjectOutputStream(sock.getOutputStream());
		objectInputStream = new ObjectInputStream(sock.getInputStream());
		ss.close();
	}
	
	public void connect(String host) throws IOException, SecurityException, IllegalArgumentException,
	UnknownHostException {
		sock = new Socket(host, port);
		objectOutputStream = new ObjectOutputStream(sock.getOutputStream());
		objectInputStream = new ObjectInputStream(sock.getInputStream());
	}
	
	public void sendMsg(Object obj) throws IOException {
		objectOutputStream.writeObject(obj);
	}
	
	public Object recvMsg() throws IOException, ClassNotFoundException {
		return objectInputStream.readObject();
	}
	
	public void closeConn() throws IOException {
		if ((!closed) && (objectOutputStream != null) && (objectInputStream != null) && (sock != null)) {
			objectOutputStream.close();
			objectInputStream.close();
			sock.close();
			closed = true;
		}
	}
}
