<!doctype html>
<html>
    <head>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,900&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <style>
            body {
                font-family: 'Roboto', sans-serif;
                padding: 0;
                margin: 0;
                color: #774F38;
                background: #C5E0DC;
            }
            .time {
                color: #774F38;
            }
            .message-head {
                color: #E08E79;
                font-weight: 900;
            }
            .content {
                font-weight: 400;
            }
            .message-head, .content {
                padding: 10px;
            }
        </style>
        <script>
            function toBottom(){
                window.scrollTo(0, document.body.scrollHeight);
            }
        </script>
    </head>
    <body onload='toBottom()'>
    <#list messages as message>
        <div class="message">
            <div class="message-head">
                <span class="time">${message.created}</span>
                <#escape x as x?html>${message.title}</#escape>
            </div>
            <div class="content">
                <#escape x as x?html?replace('\n', '<br>')>${message.content}</#escape>
            </div>
            <hr/>
        </div>
    </#list>
    </body>
</html>