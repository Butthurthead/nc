package ru.butthurthead.typoicq;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.butthurthead.typoicq.conf.ConfManager;
import ru.butthurthead.typoicq.conf.Configuration;
import ru.butthurthead.typoicq.domain.JDBCMessageRepository;
import ru.butthurthead.typoicq.domain.Message;
import ru.butthurthead.typoicq.domain.MessageRepository;
import ru.butthurthead.typoicq.migration.InitialMigration;
import ru.butthurthead.typoicq.migration.Migration;
import ru.butthurthead.typoicq.network.Client;
import ru.butthurthead.typoicq.network.Server;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static ru.butthurthead.typoicq.util.FileEncryptor.decrypt;
import static ru.butthurthead.typoicq.util.FileEncryptor.encrypt;

/**
 * Holds app state and provide methods to change it
 */
public class Kernel {

    private Connection dbConnection;
    private Configuration conf;
    private String password;
    private MessageRepository messageRepository;
    private ObservableList<Message> messages;
    private Client client;
    private Server server;

    public Kernel(Configuration conf) {
        this.conf = conf;
    }

    /**
     * Sets password for encryption/decryption
     *
     * @param password encryption/decryption password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Prepares the database file and connects to it
     *
     * @throws IOException            if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void init() throws IOException, ClassNotFoundException, SQLException {
        File databaseFile = new File(conf.getDatabase().getFile());
        File databaseTempFile = new File(conf.getDatabase().getTemporaryFile());
        if (databaseFile.exists()) {
            decrypt(password, databaseFile, databaseTempFile);
        }

        Class.forName(conf.getDatabase().getDriver());
        dbConnection = DriverManager.getConnection(conf.getDatabase().getConnection());

        Migration migration = new InitialMigration(dbConnection);
        migration.up();

        messageRepository = new JDBCMessageRepository(dbConnection);
        messages = FXCollections.observableArrayList(messageRepository.getAll());
    }

    /**
     * Closes connection to the database, encrypts database file and deletes decrypted file
     *
     * @throws IOException   if an I/O error occurs
     * @throws SQLException  if a database access error occurs
     * @throws JAXBException if failed to save configuration
     */
    public void terminate() throws IOException, SQLException, JAXBException {
        disconnectFromServer();
        stopServer();
        dbConnection.close();

        File databaseFile = new File(conf.getDatabase().getFile());
        File databaseTempFile = new File(conf.getDatabase().getTemporaryFile());
        if (databaseTempFile.exists()) {
            encrypt(password, databaseTempFile, databaseFile);
            Files.delete(databaseTempFile.toPath());
        }

        ConfManager.save(conf, new FileOutputStream("config.xml"));
    }

    /**
     * Saves message to the repository and sends it to the server if connection is set
     *
     * @param message the message to send
     * @throws SQLException  if failed to save the message
     * @throws JAXBException if failed to marshall the message
     * @throws IOException   if failed to write the message to the socket
     */
    public void sendMessage(Message message) throws SQLException, IOException, JAXBException {
        receiveMessage(message);

        if (client != null && client.isRunning()) {
            client.sendMessage(message);
        }
    }

    /**
     * Starts server
     *
     * @param port server port
     * @throws IOException if failed to start the server
     */
    public void startServer(int port) throws IOException {
        conf.getServer().setPort(port);
        server = new Server();
        server.start(port);
        new Thread(server, "server thread").start();
        while (!server.isRunning()) ; // wait for server
        connectToServer("localhost", port);
    }

    /**
     * Stops the server
     *
     * @throws IOException if failed to stop the server
     */
    public void stopServer() throws IOException {
        if (server != null) {
            client.disconnect();
            server.stop();
        }
    }

    /**
     * Connects to the server
     *
     * @param host server host
     * @param port server port
     * @throws IOException if failed to connect to the server
     */
    public void connectToServer(String host, int port) throws IOException {
        conf.getClient().setHost(host);
        conf.getClient().setPort(port);
        client = new Client(c -> {
            while (c.next()) {
                for (Message message : c.getAddedSubList()) {
                    if (!messages.contains(message)) {
                        messages.add(message);
                    }
                }
            }
        });

        client.connect(host, port);
        new Thread(client, "client thread").start();
    }

    /**
     * Disconnects form the server
     *
     * @throws IOException if failed to disconnect from the server
     */
    public void disconnectFromServer() throws IOException {
        if (client != null)
            client.disconnect();
    }

    /**
     * Saves a message to the repository
     *
     * @param message the message to save
     * @throws SQLException if failed to save the message
     */
    public void receiveMessage(Message message) throws SQLException {
        if (!messages.contains(message)) {
            messages.add(message);
            messageRepository.save(message);
        }
    }

    /**
     * Gets messages list
     *
     * @return list which contains all the messages
     */
    public ObservableList<Message> getMessages() {
        return messages;
    }

}
