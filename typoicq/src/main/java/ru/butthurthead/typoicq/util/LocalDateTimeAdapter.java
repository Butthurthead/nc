package ru.butthurthead.typoicq.util;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Adapts LocalDateTime type for marshalling
 */
public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * Converts String to LocalDateTime
     *
     * @param v string to convert
     * @return LocalDateTime object representation of given string
     * @throws DateTimeParseException if the text cannot be parsed
     */
    @Override
    public LocalDateTime unmarshal(String v) {
        return LocalDateTime.parse(v, formatter);
    }

    /**
     * Converts LocalDateTime object to String
     *
     * @param v object to convert
     * @return string representation of given LocalDateTime object
     * @throws DateTimeException if an error occurs during printing
     */
    @Override
    public String marshal(LocalDateTime v) {
        return v.format(formatter);
    }
}
