package ru.butthurthead.typoicq.util;

import org.jasypt.util.binary.BasicBinaryEncryptor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Simple wrapper for jasypt library
 */
public class FileEncryptor {

    /**
     * Decrypts file
     *
     * @param password   the password to use for decryption
     * @param inputFile  the encrypted file to be decrypted
     * @param outputFile the file to marshall results to
     * @throws IOException if an I/O error occurs
     */
    public static void decrypt(String password, File inputFile, File outputFile)
            throws IOException {
        BasicBinaryEncryptor encryptor = new BasicBinaryEncryptor();
        encryptor.setPassword(password);

        try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
            byte[] decrypted = encryptor.decrypt(Files.readAllBytes(inputFile.toPath()));
            outputStream.write(decrypted);
        }
    }

    /**
     * Encrypts file
     *
     * @param password   the password to use for encryption
     * @param inputFile  the decrypted file to be encrypted
     * @param outputFile the file to marshall results to
     * @throws IOException if an I/O error occurs
     */
    public static void encrypt(String password, File inputFile, File outputFile)
            throws IOException {
        BasicBinaryEncryptor encryptor = new BasicBinaryEncryptor();
        encryptor.setPassword(password);

        try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
            byte[] decrypted = Files.readAllBytes(inputFile.toPath());
            byte[] encrypted = encryptor.encrypt(decrypted);
            outputStream.write(encrypted);
        }
    }
}
