package ru.butthurthead.typoicq.domain;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * Provides access to messages stored in JDBC source
 */
public class JDBCMessageRepository implements MessageRepository {

    private final static String CREATED_COLUMN = "created";
    private final static String TITLE_COLUMN = "title";
    private final static String CONTENT_COLUMN = "content";

    private Connection connection;

    /**
     * Initializes a new instance of the JDBCMessageRepository
     *
     * @param connection a connection with a specific database
     */
    public JDBCMessageRepository(Connection connection) {
        this.connection = connection;
    }

    /**
     * Retrieves all messages from the database
     *
     * @return all messages from the database
     * @throws SQLException if a database access error occurs
     */
    @Override
    public List<Message> getAll() throws SQLException {
        List<Message> messages = new LinkedList<>();
        String SQL = "SELECT * FROM messages";

        try (Statement statement = connection.createStatement()) {
            ResultSet results = statement.executeQuery(SQL);
            while (results.next()) {
                LocalDateTime created = results.getTimestamp(CREATED_COLUMN).toLocalDateTime();
                String title = results.getString(TITLE_COLUMN);
                String content = results.getString(CONTENT_COLUMN);
                messages.add(Message.create(created, title, content));
            }
        }

        return messages;
    }

    /**
     * Saves message to the database
     *
     * @param message a message to save
     * @throws SQLException if a database access error occurs
     */
    @Override
    public void save(Message message) throws SQLException {
        String SQL = "INSERT INTO messages VALUES (?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(SQL)) {
            statement.setTimestamp(1, Timestamp.valueOf(message.getCreated()));
            statement.setString(2, message.getTitle());
            statement.setString(3, message.getContent());
            statement.execute();
        }
    }
}
