package ru.butthurthead.typoicq.domain;

import java.sql.SQLException;
import java.util.List;

/**
 * Provides access to messages stored somewhere
 */
public interface MessageRepository {

    /**
     * Returns all messages from some source
     *
     * @return all messages
     * @throws SQLException if a database access error occurs
     */
    List<Message> getAll() throws SQLException;

    /**
     * Saves message to the some store
     *
     * @param message a message to save
     * @throws SQLException if a database access error occurs
     */
    void save(Message message) throws SQLException;
}
