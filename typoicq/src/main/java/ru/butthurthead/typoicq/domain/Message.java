package ru.butthurthead.typoicq.domain;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import ru.butthurthead.typoicq.util.LocalDateTimeAdapter;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;

/**
 * Represents a chat message
 */
@XmlRootElement
@XmlType(propOrder = {"created", "title", "content"})
public final class Message {
    private final StringProperty title = new SimpleStringProperty();
    private final StringProperty content = new SimpleStringProperty();
    private final ObjectProperty<LocalDateTime> created = new SimpleObjectProperty<>();

    /**
     * Creates message object
     *
     * @param created message created time
     * @param title   message title
     * @param content message content
     * @return message object
     */
    public static Message create(LocalDateTime created, String title, String content) {
        Message message = new Message();

        message.setCreated(created);
        message.setTitle(title);
        message.setContent(content);

        return message;
    }

    /**
     * Gets message title
     *
     * @return message title
     */
    public String getTitle() {
        return title.get();
    }

    /**
     * Sets message title
     *
     * @param title message title
     */
    public void setTitle(String title) {
        this.title.set(title);
    }

    /**
     * Gets property of message title
     *
     * @return property of message title
     */
    public StringProperty titleProperty() {
        return title;
    }

    /**
     * Returns message content
     *
     * @return message content
     */
    public String getContent() {
        return content.get();
    }

    /**
     * Sets message content
     *
     * @param content message content
     */
    public void setContent(String content) {
        this.content.set(content);
    }

    /**
     * Gets property of message content
     *
     * @return property of message content
     */
    public StringProperty contentProperty() {
        return content;
    }

    /**
     * Gets message created time
     *
     * @return message created time
     */
    public LocalDateTime getCreated() {
        return created.get();
    }

    /**
     * Sets message created time
     *
     * @param created message created time
     */
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public void setCreated(LocalDateTime created) {
        this.created.set(created.withNano(0));
    }

    /**
     * Gets property of message created time
     *
     * @return property of message created time
     */
    public ObjectProperty<LocalDateTime> createdProperty() {
        return created;
    }

    /**
     * Tests the equality of objects
     *
     * @param obj object to test
     * @return true if objects are equal by pointer or fields; otherwise returns false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Message other = (Message) obj;
        return getCreated().equals(other.getCreated())
                && getTitle().equals(other.getTitle())
                && getContent().equals(other.getContent());
    }
}
