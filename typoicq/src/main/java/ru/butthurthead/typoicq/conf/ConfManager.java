package ru.butthurthead.typoicq.conf;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.InputStream;
import java.io.OutputStream;

public class ConfManager {

    private static final Unmarshaller unmarshaller;
    private static final Marshaller marshaller;

    static {
        try {
            JAXBContext context = JAXBContext.newInstance(Configuration.class);

            unmarshaller = context.createUnmarshaller();
            marshaller = context.createMarshaller();

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(Configuration.class.getResource("schema.xsd"));

            unmarshaller.setSchema(schema);
            marshaller.setSchema(schema);
        } catch (JAXBException | SAXException e) {
            throw new RuntimeException(e);
        }
    }

    public static Configuration load(InputStream inputStream) throws JAXBException {
        return (Configuration) unmarshaller.unmarshal(inputStream);
    }

    public static void save(Configuration conf, OutputStream outputStream) throws JAXBException {
        marshaller.marshal(conf, outputStream);
    }
}
