//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.12.09 at 01:57:21 AM MSK 
//


package ru.butthurthead.typoicq.conf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="database" type="{}databaseConfiguration"/>
 *         &lt;element name="server" type="{}serverConfiguration"/>
 *         &lt;element name="client" type="{}clientConfiguration"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "database",
    "server",
    "client"
})
@XmlRootElement(name = "configuration")
public class Configuration {

    @XmlElement(required = true)
    protected DatabaseConfiguration database;
    @XmlElement(required = true)
    protected ServerConfiguration server;
    @XmlElement(required = true)
    protected ClientConfiguration client;

    /**
     * Gets the value of the database property.
     * 
     * @return
     *     possible object is
     *     {@link DatabaseConfiguration }
     *     
     */
    public DatabaseConfiguration getDatabase() {
        return database;
    }

    /**
     * Sets the value of the database property.
     * 
     * @param value
     *     allowed object is
     *     {@link DatabaseConfiguration }
     *     
     */
    public void setDatabase(DatabaseConfiguration value) {
        this.database = value;
    }

    /**
     * Gets the value of the server property.
     * 
     * @return
     *     possible object is
     *     {@link ServerConfiguration }
     *     
     */
    public ServerConfiguration getServer() {
        return server;
    }

    /**
     * Sets the value of the server property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServerConfiguration }
     *     
     */
    public void setServer(ServerConfiguration value) {
        this.server = value;
    }

    /**
     * Gets the value of the client property.
     * 
     * @return
     *     possible object is
     *     {@link ClientConfiguration }
     *     
     */
    public ClientConfiguration getClient() {
        return client;
    }

    /**
     * Sets the value of the client property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClientConfiguration }
     *     
     */
    public void setClient(ClientConfiguration value) {
        this.client = value;
    }

}
