package ru.butthurthead.typoicq.ui;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

import static javafx.scene.control.ButtonBar.ButtonData;

/**
 * Contains helper methods to construct app's dialogs
 */
public class DialogFactory {

    /**
     * Creates login dialog
     *
     * @return the dialog
     */
    public static Dialog<String> createLoginDialog() {
        Dialog<String> dialog = new Dialog<>();

        dialog.setTitle("Аутентификация");
        dialog.setHeaderText(null);

        // Set the button types
        ButtonType loginButtonType = new ButtonType("Вход", ButtonData.OK_DONE);
        ButtonType exitButtonType = new ButtonType("Выход", ButtonData.CANCEL_CLOSE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, exitButtonType);

        // Create the password label and field
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20));

        PasswordField password = new PasswordField();
        password.setPromptText("Пароль");

        grid.add(new Label("Пароль:"), 0, 0);
        grid.add(password, 1, 0);

        // Enable/Disable login button depending on whether a password was entered
        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(true);

        password.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.isEmpty());
        });

        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return password.getText();
            }
            return null;
        });

        return dialog;
    }

    /**
     * Creates start server dialog
     *
     * @param port default port
     * @return the dialog
     */
    public static Dialog<Integer> createStartServerDialog(int port) {
        Dialog<Integer> dialog = new Dialog<>();

        dialog.setTitle("Создание сервера");
        dialog.setHeaderText(null);

        // Set the button types
        ButtonType createButtonType = new ButtonType("Создать", ButtonData.OK_DONE);
        ButtonType cancelButtonType = new ButtonType("Отмена", ButtonData.CANCEL_CLOSE);
        dialog.getDialogPane().getButtonTypes().addAll(createButtonType, cancelButtonType);

        // Create the password label and field
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20));

        TextField portField = new TextField();
        portField.setText(Integer.toString(port));

        grid.add(new Label("Порт:"), 0, 0);
        grid.add(portField, 1, 0);

        // Enable/Disable login button depending on whether a password was entered
        Node createButton = dialog.getDialogPane().lookupButton(createButtonType);

        portField.textProperty().addListener((observable, oldValue, newValue) -> {
            createButton.setDisable(newValue.isEmpty());
        });

        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == createButtonType) {
                return Integer.parseInt(portField.getText());
            }
            return null;
        });

        return dialog;
    }

    /**
     * Creates connect to server dialog
     *
     * @param defaultHost default hostname
     * @param defaultPort default port
     * @return the dialog
     */
    public static Dialog<Pair<String, Integer>> createConnectDialog(String defaultHost, int defaultPort) {
        Dialog<Pair<String, Integer>> dialog = new Dialog<>();

        dialog.setTitle("Подключение к серверу");
        dialog.setHeaderText(null);

        // Set the button types
        ButtonType connectButtonType = new ButtonType("Подключиться", ButtonData.OK_DONE);
        ButtonType cancelButtonType = new ButtonType("Отмена", ButtonData.CANCEL_CLOSE);
        dialog.getDialogPane().getButtonTypes().addAll(connectButtonType, cancelButtonType);

        // Create the password label and field
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20));

        TextField hostTextField = new TextField();
        hostTextField.setText(defaultHost);

        TextField portTextField = new TextField();
        portTextField.setText(Integer.toString(defaultPort));

        grid.add(new Label("Адрес:"), 0, 0);
        grid.add(hostTextField, 1, 0);
        grid.add(new Label("Порт:"), 0, 1);
        grid.add(portTextField, 1, 1);

        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == connectButtonType) {
                return new Pair<>(hostTextField.getText(),
                        Integer.parseInt(portTextField.getText()));
            }
            return null;
        });

        return dialog;
    }

    /**
     * Creates error dialog
     *
     * @param message error message
     * @return the dialog
     */
    public static Alert createErrorDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Опаньки");
        alert.setHeaderText("Опаньки!");
        alert.setContentText(message);

        return alert;
    }
}
