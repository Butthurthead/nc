package ru.butthurthead.typoicq.ui;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.butthurthead.typoicq.Kernel;
import ru.butthurthead.typoicq.conf.ClientConfiguration;
import ru.butthurthead.typoicq.conf.Configuration;
import ru.butthurthead.typoicq.domain.Message;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class MainController {

    private static final Logger logger = LogManager.getLogger();

    @FXML
    private Menu clientMenu;

    @FXML
    private Menu serverMenu;

    @FXML
    private MenuItem connectToServerMenuItem;

    @FXML
    private MenuItem disconnectFromServerMenuItem;

    @FXML
    private MenuItem startServerMenuItem;

    @FXML
    private MenuItem stopServerMenuItem;

    @FXML
    private WebView chatWebView;

    @FXML
    private TextField titleTextField;

    @FXML
    private TextArea contentTextArea;

    private Template template;
    private Configuration configuration;
    private Kernel kernel;

    public void setTemplate(Template template) {
        this.template = template;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public void setKernel(Kernel kernel) {
        this.kernel = kernel;
    }

    public void init() {
        if (kernel == null || template == null || configuration == null)
            throw new IllegalStateException();

        kernel.getMessages().addListener((InvalidationListener) (c) -> {
            updateChatWebView();
        });

        updateChatWebView();
        disconnectFromServerMenuItem.setDisable(true);
        stopServerMenuItem.setDisable(true);
    }

    @FXML
    private void handleExitAction(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    private void handleConnectAction(ActionEvent event) {
        ClientConfiguration clientConfiguration = configuration.getClient();
        Optional<Pair<String, Integer>> results = DialogFactory.createConnectDialog(
                clientConfiguration.getHost(), clientConfiguration.getPort()).showAndWait();
        if (results.isPresent()) {
            Pair<String,Integer> pair = results.get();
            try {
                kernel.connectToServer(pair.getKey(), pair.getValue());
                connectToServerMenuItem.setDisable(true);
                disconnectFromServerMenuItem.setDisable(false);
                serverMenu.setDisable(true);
            } catch (IOException e) {
                logger.info("failed to connect to the server ({}:{}): {} {}",
                        pair.getKey(), pair.getValue(), e.getClass(), e.getMessage());
                DialogFactory.createErrorDialog("Неудалось подключиться к серверу").showAndWait();
            }
        }
    }

    @FXML
    private void handleDisconnectAction(ActionEvent event) {
        try {
            kernel.disconnectFromServer();
            connectToServerMenuItem.setDisable(false);
            disconnectFromServerMenuItem.setDisable(true);
            serverMenu.setDisable(false);
        } catch (IOException e) {
            logger.info("failed to disconnect from server: {} {}", e.getClass(), e.getMessage());
            DialogFactory.createErrorDialog("Неудалось отключиться от сервера :(").showAndWait();
        }
    }

    @FXML
    private void handleStartServerAction(ActionEvent event) {
        Optional<Integer> results = DialogFactory.createStartServerDialog(
                configuration.getServer().getPort()).showAndWait();
        if (results.isPresent()) {
            try {
                kernel.startServer(results.get());
                startServerMenuItem.setDisable(true);
                stopServerMenuItem.setDisable(false);
                clientMenu.setDisable(true);
            } catch (IOException e) {
                logger.error("failed to start the server: {} {}", e.getClass(), e.getMessage());
                DialogFactory.createErrorDialog("Неудалось запустить сервер").showAndWait();
            }
        }
    }

    @FXML
    private void handleStopServerAction(ActionEvent event) {
        try {
            kernel.stopServer();
            startServerMenuItem.setDisable(false);
            stopServerMenuItem.setDisable(true);
            clientMenu.setDisable(false);
        } catch (IOException e) {
            logger.error("failed to stop the server: {} {}", e.getClass(), e.getMessage());
            DialogFactory.createErrorDialog("Неудалось остановить сервер").showAndWait();
        }
    }

    @FXML
    private void handleSendMessageAction(ActionEvent event) {
        try {
            kernel.sendMessage(Message.create(
                    LocalDateTime.now(), titleTextField.getText(), contentTextArea.getText()
            ));
            titleTextField.clear();
            contentTextArea.clear();
        } catch (SQLException | IOException | JAXBException e) {
            logger.error("failed to send the message: {} {}", e.getClass(), e.getMessage());
            DialogFactory.createErrorDialog("Неудалось отправить сообщение").showAndWait();
        }
    }

    private void updateChatWebView() {
        Map<String, Object> root = new HashMap<>();
        root.put("messages", kernel.getMessages());

        Writer out = new StringWriter();
        try {
            template.process(root, out);
        } catch (TemplateException | IOException e) {
            logger.error("failed to generate a template: " + e.getMessage());
        }

        chatWebView.getEngine().loadContent(out.toString());
    }
}
