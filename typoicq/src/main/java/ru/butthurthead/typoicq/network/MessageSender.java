package ru.butthurthead.typoicq.network;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.butthurthead.typoicq.domain.Message;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

/**
 * Sends messages from message queue to the clients
 */
class MessageSender implements Runnable {

    private static final Logger logger = LogManager.getLogger();

    private List<Socket> clientsQueue;
    private List<Message> messagesQueue;
    private boolean stopping;
    private boolean stopped;

    public MessageSender(List<Socket> clientsQueue, List<Message> messagesQueue) {
        this.clientsQueue = clientsQueue;
        this.messagesQueue = messagesQueue;
    }

    /**
     * Sends messages from message queue to the clients
     */
    @Override
    public void run() {
        while (!stopping) {
            for (Message message : messagesQueue) {
                for (Socket client : clientsQueue) {
                    if (!client.isClosed()) {
                        try {
                            Protocol.sendMessage(message, client.getOutputStream());
                            logger.trace("the message was sent to the client");
                        } catch (IOException | JAXBException e) {
                            logger.error("failed to send the message to the client: " + e.getMessage());
                        }
                    }
                }
                messagesQueue.remove(message);
            }
        }
        synchronized (this) {
            stopped = true;
            notifyAll();
        }
    }

    /**
     * Stops the sender
     */
    public synchronized void stop() {
        logger.trace("stopping message sender");
        stopping = true;
        while (!stopped) {
            try {
                wait();
            } catch (InterruptedException ignore) {
            }
        }
        logger.trace("message sender stopped");
    }
}
