package ru.butthurthead.typoicq.network;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.butthurthead.typoicq.domain.Message;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

/**
 * Receives messages on the server and places them to the queue
 */
class MessageReceiver implements Runnable {

    private static final Logger logger = LogManager.getLogger();

    private List<Socket> clientsQueue;
    private List<Message> messagesQueue;
    private boolean stopping = false;
    private boolean stopped = false;

    public MessageReceiver(List<Socket> clientsQueue, List<Message> messagesQueue) {
        this.clientsQueue = clientsQueue;
        this.messagesQueue = messagesQueue;
    }

    /**
     * Check clients for input and read it.
     */
    @Override
    public void run() {
        while (!stopping) {
            for (Socket client : clientsQueue) {
                if (!client.isClosed()) {
                    try {
                        InputStream inputStream = client.getInputStream();
                        if (inputStream.available() > 0) {
                            Message msg = Protocol.receiveMessage(inputStream);
                            messagesQueue.add(msg);
                        }
                    } catch (IOException | JAXBException e) {
                        logger.error("failed to receive the message: " + e.getMessage());
                    }
                }
            }
        }
        synchronized (this){
            stopped = true;
            notifyAll();
        }
    }

    /**
     * Stops the receiver
     */
    public synchronized void stop() {
        logger.trace("stopping message receiver");
        stopping = true;
        while (!stopped) {
            try {
                wait();
            } catch (InterruptedException ignore){
            }
        }
        logger.trace("message receiver stopped");
    }
}
