package ru.butthurthead.typoicq.network;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.butthurthead.typoicq.domain.Message;
import ru.butthurthead.typoicq.util.JaxbParser;

import javax.xml.bind.JAXBException;
import java.io.*;

/**
 * Simple protocol for the messenger
 */
class Protocol {

    private static final Logger logger = LogManager.getLogger();

    private final static byte END_OF_MESSAGE_BYTE = 0;

    /**
     * Writes message to the output stream
     *
     * @param message the message to send
     * @param out     the output stream
     * @throws IOException   if failed to write to the stream
     * @throws JAXBException if failed to marshall the message
     */
    public static void sendMessage(Message message, OutputStream out) throws JAXBException, IOException {
        JaxbParser.marshall(message, out);
        out.write(END_OF_MESSAGE_BYTE);
    }

    /**
     * Receives message from server
     *
     * @param in the input stream
     * @return the message received from the server
     * @throws JAXBException if failed to unmarshall the message
     * @throws IOException   if failed to read from the stream
     */
    public static Message receiveMessage(InputStream in) throws JAXBException, IOException {
        int b = -1;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        while (b != END_OF_MESSAGE_BYTE) {
            if (in.available() > 0) {
                b = in.read();
                if (b != END_OF_MESSAGE_BYTE) {
                    out.write(b);
                }
            }
        }
        logger.trace("read " + out.size() + "bytes");
        return (Message) JaxbParser.unmarshall(Message.class, new ByteArrayInputStream(out.toByteArray()));
    }
}
