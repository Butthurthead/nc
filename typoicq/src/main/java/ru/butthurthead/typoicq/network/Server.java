package ru.butthurthead.typoicq.network;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.butthurthead.typoicq.domain.Message;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Accepts connections from clients and adds them to the queue
 */
public class Server implements Runnable {

    private static final Logger logger = LogManager.getLogger();

    private ServerSocket serverSocket;
    private List<Socket> clientsQueue = new CopyOnWriteArrayList<>();
    private List<Message> messagesQueue = new CopyOnWriteArrayList<>();

    private MessageReceiver receiver;
    private MessageSender sender;

    /**
     * Starts the server at the given port
     *
     * @param port server port
     * @throws IOException           if failed to start the server
     * @throws IllegalStateException if server is already started
     */
    public void start(int port) throws IOException {
        if (serverSocket == null || serverSocket.isClosed()) {
            serverSocket = new ServerSocket(port);
            logger.trace("server started at port " + port);
            receiver = new MessageReceiver(clientsQueue, messagesQueue);
            new Thread(receiver).start();
            logger.trace("message receiver started");
            sender = new MessageSender(clientsQueue, messagesQueue);
            new Thread(sender).start();
            logger.trace("message sender started");
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * Stops the server
     *
     * @throws IOException if failed to stop the server
     */
    public void stop() throws IOException {
        if (serverSocket != null && !serverSocket.isClosed()) {
            receiver.stop();
            sender.stop();
            serverSocket.close();
            logger.trace("server stopped");
        }
    }

    /**
     * Listen for connections and add them to the queue
     */
    @Override
    public void run() {
        while (!serverSocket.isClosed()) {
            try {
                Socket clientSocket = serverSocket.accept();
                clientsQueue.add(clientSocket);
                logger.trace("client connected");
            } catch (IOException ignore) {
            }
        }
    }

    /**
     * Returns server state
     *
     * @return true if running otherwise false
     */
    public boolean isRunning() {
        return serverSocket != null && !serverSocket.isClosed();
    }
}
