package ru.butthurthead.typoicq.network;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.butthurthead.typoicq.domain.Message;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Client for the server. Connects, sends and retrieves message from the server.
 */
public class Client implements Runnable {

    private static final Logger logger = LogManager.getLogger();

    private ObservableList<Message> messages = FXCollections.observableArrayList();
    private Socket socket;
    private boolean stopping = false;
    private boolean stopped = false;

    /**
     * Constructs new client.
     *
     * @param listener listener for new messages
     */
    public Client(ListChangeListener<Message> listener) {
        messages.addListener(listener);
    }

    /**
     * Connects to the server
     *
     * @throws IOException           if failed to connect to the server
     * @throws IllegalStateException if connected to a server already
     */
    public void connect(String host, int port) throws IOException {
        if (socket == null || socket.isClosed()) {
            socket = new Socket(host, port);
            logger.trace("connected to server");
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * Disconnects from the server. If already disconnected does nothing.
     *
     * @throws IOException if failed to close the connection
     */
    public void disconnect() throws IOException {
        if (socket != null && !socket.isClosed()) {
            stopping = true;
            logger.trace("disconnecting");
            while (stopped != true) ;
            socket.close();
            logger.trace("disconnected from server");
        }
    }

    /**
     * Sends message to the server
     *
     * @param message the message to send
     * @throws IOException   if failed to write to the socket
     * @throws JAXBException if failed to marshal the message
     */
    public void sendMessage(Message message) throws IOException, JAXBException {
        OutputStream out = socket.getOutputStream();
        Protocol.sendMessage(message, out);
        logger.trace("the message was sent to the server");
    }

    /**
     * Returns client state
     *
     * @return true if connected to server otherwise false
     */
    public boolean isRunning() {
        return socket != null && !socket.isClosed();
    }

    /**
     * Reads messages from the server
     */
    @Override
    public void run() {
        try (InputStream inputStream = socket.getInputStream()) {
            while (socket.isConnected() && !stopping) {
                while (inputStream.available() > 0) {
                    try {
                        Message msg = Protocol.receiveMessage(inputStream);
                        Platform.runLater(() -> messages.add(msg));
                    } catch (JAXBException | IOException e) {
                        logger.error("failed to receive the message: " + e.getMessage());
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        stopped = true;
    }
}
