package ru.butthurthead.typoicq.migration;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Creates initial database structure
 */
public class InitialMigration implements Migration {

    private Connection connection;

    public InitialMigration(Connection connection) {
        this.connection = connection;
    }

    /**
     * Creates initial database structure
     *
     * @throws SQLException if failed to execute a query
     */
    @Override
    public void up() throws SQLException {
        String SQL = "CREATE TABLE IF NOT EXISTS messages (" +
                "created BLOB NOT NULL," +
                "title TEXT NOT NULL," +
                "content TEXT NOT NULL" +
                ")";

        try (Statement statement = connection.createStatement()) {
            statement.execute(SQL);
        }
    }
}
