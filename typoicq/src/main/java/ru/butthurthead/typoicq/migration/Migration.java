package ru.butthurthead.typoicq.migration;

import java.sql.SQLException;

/**
 * Represents a database migration
 */
public interface Migration {

    /**
     * Installs the migration
     *
     * @throws SQLException if the process failed
     */
    void up() throws SQLException;
}
