package ru.butthurthead.typoicq;

import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import ru.butthurthead.typoicq.conf.ConfManager;
import ru.butthurthead.typoicq.conf.Configuration;
import ru.butthurthead.typoicq.ui.DialogFactory;
import ru.butthurthead.typoicq.ui.MainController;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.Optional;

public class TypoICQ extends Application {

    private static final Logger logger = LogManager.getLogger();

    private Configuration configuration;
    private Kernel kernel;
    private Template messageLogTemplate;

    @Override
    public void start(Stage primaryStage) {
        initialize();
        Optional<String> password = askPassword();
        if (password.isPresent()) {
            kernel.setPassword(password.get());
        } else {
            System.exit(1);
        }

        try {
            kernel.init();
        } catch (EncryptionOperationNotPossibleException e) {
            DialogFactory.createErrorDialog("Некорректный пароль").showAndWait();
            File databaseTempFile = new File(configuration.getDatabase().getTemporaryFile());
            try {
                Files.delete(databaseTempFile.toPath());
            } catch (IOException ignore) {
            }
            System.exit(1);
        } catch (IOException | SQLException | ClassNotFoundException e) {
            logger.error("failed to initialize the application: {} {}", e.getClass(), e.getMessage());
            DialogFactory.createErrorDialog("Неудалось инициализировать приложение").showAndWait();
            System.exit(1);
        }

        FXMLLoader loader = new FXMLLoader();
        try {
            InputStream mainFxml = MainController.class.getResourceAsStream("main.fxml");
            Parent uiContainer = loader.load(mainFxml);
            Scene scene = new Scene(uiContainer, 800, 600);
            primaryStage.setScene(scene);
        } catch (IOException e) {
            logger.error("failed to load window layout: {} {}", e.getClass(), e.getMessage());
            DialogFactory.createErrorDialog("Неудалось загрузить интерфейс пользователя").showAndWait();
            System.exit(1);
        }

        MainController controller = loader.getController();
        controller.setConfiguration(configuration);
        controller.setKernel(kernel);
        controller.setTemplate(messageLogTemplate);
        controller.init();

        primaryStage.setTitle("Typo ICQ");
        primaryStage.show();
    }

    @Override
    public void stop() {
        try {
            kernel.terminate();
        } catch (IOException | SQLException | JAXBException e) {
            logger.error("failed to terminate the application: {} {}", e.getClass(), e.getMessage());
            DialogFactory.createErrorDialog("Неуадалось корректно завершить приложение").showAndWait();
        }
    }

    private void initialize() {
        try {
            initKernel();
            initFreemarker();
        } catch (IOException | JAXBException e) {
            logger.error("failed to initialize components: {} {}", e.getClass(), e.getMessage());

            DialogFactory.createErrorDialog("Неудалось инициализровать компоненты приложения").showAndWait();

            Platform.exit();
        }
    }

    private Optional<String> askPassword() {
        Dialog<String> dialog = DialogFactory.createLoginDialog();
        return dialog.showAndWait();
    }

    private void initKernel() throws FileNotFoundException, JAXBException {
        File configFile = new File("config.xml");
        InputStream configInputStream;
        if (configFile.exists()) {
            logger.trace("loading custom configuration");
            configInputStream = new FileInputStream(configFile);
        } else {
            logger.trace("loading default configuration");
            configInputStream = Configuration.class.getResourceAsStream("default.xml");
        }
        configuration = ConfManager.load(configInputStream);
        kernel = new Kernel(configuration);
    }

    private void initFreemarker() throws IOException {
        freemarker.template.Configuration freemarkerConfiguration;
        freemarkerConfiguration = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_23);
        freemarkerConfiguration.setClassForTemplateLoading(getClass(), "ui");
        freemarkerConfiguration.setDefaultEncoding("UTF-8");
        freemarkerConfiguration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

        messageLogTemplate = freemarkerConfiguration.getTemplate("messagelog.ftl");
    }

}
