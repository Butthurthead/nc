package ru.butthurthead.typoicq.conf;

import org.junit.Test;

import java.io.InputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ConfigurationTest {

    private static final InputStream validConfiguration =
            ConfigurationTest.class.getResourceAsStream("validConfiguration.xml");

    @Test
    public void testValid() throws Exception {
        Configuration conf = ConfManager.load(validConfiguration);

        assertThat(conf.getDatabase().getDriver(), is("org.sqlite.JDBC"));
        assertThat(conf.getDatabase().getConnection(), is("jdbc:sqlite:typoicq.temp"));
        assertThat(conf.getDatabase().getFile(), is("typoicq.db"));
        assertThat(conf.getDatabase().getTemporaryFile(), is("typoicq.db.temp"));

        assertThat(conf.getServer().getPort(), is(6610));

        assertThat(conf.getClient().getHost(), is("localhost"));
        assertThat(conf.getClient().getPort(), is(6610));
    }
}
