package ru.butthurthead.typoicq.conf;

public class ConfigurationFactory {

    public static final String CONNECTION = "jdbc:sqlite:";
    public static final String DRIVER = "org.sqlite.JDBC";

    public static final int SERVER_PORT = 6610;

    public static final String CLIENT_HOST = "localhost";
    public static final int CLIENT_PORT = 6610;

    public static Configuration createConfiguration(String file, String tempFile) {
        Configuration conf = new Configuration();

        DatabaseConfiguration dbConf = new DatabaseConfiguration();
        dbConf.setConnection(CONNECTION + file);
        dbConf.setDriver(DRIVER);
        dbConf.setFile(file);
        dbConf.setTemporaryFile(tempFile);

        ServerConfiguration servConf = new ServerConfiguration();
        servConf.setPort(SERVER_PORT);

        ClientConfiguration clientConf = new ClientConfiguration();
        clientConf.setHost(CLIENT_HOST);
        clientConf.setPort(CLIENT_PORT);

        conf.setDatabase(dbConf);
        conf.setServer(servConf);
        conf.setClient(clientConf);

        return conf;
    }
}
