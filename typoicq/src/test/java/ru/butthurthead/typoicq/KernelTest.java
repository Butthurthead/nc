package ru.butthurthead.typoicq;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import ru.butthurthead.typoicq.conf.Configuration;
import ru.butthurthead.typoicq.conf.ConfigurationFactory;
import ru.butthurthead.typoicq.domain.Message;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class KernelTest {

    private static final String password = "qwerty1234";
    private static final Message messageToSend = Message.create(LocalDateTime.now(), "Title", "Content");

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private Configuration conf;

    @Before
    public void setUp() throws Exception {
        conf = ConfigurationFactory.createConfiguration(
                new File(folder.getRoot() + "notexists").toString(), folder.newFile().toString());
    }

    @Ignore("breaks configuration file")
    @Test
    public void testFlow() throws Exception {
        Kernel kernel = new Kernel(conf);
        kernel.setPassword(password);

        kernel.init();
        List<Message> messages = kernel.getMessages();
        assertThat(messages.size(), is(0));

        kernel.sendMessage(messageToSend);
        assertThat(messages.contains(messageToSend), is(true));

        kernel.terminate();
        assertThat(new File(conf.getDatabase().getTemporaryFile()).exists(), is(false));
        assertThat(new File(conf.getDatabase().getFile()).exists(), is(true));
    }

}
