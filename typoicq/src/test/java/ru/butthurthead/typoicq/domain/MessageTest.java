package ru.butthurthead.typoicq.domain;

import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

public class MessageTest {

    private static final LocalDateTime created = LocalDateTime.of(2007, 7, 7, 7, 7, 7);
    private static final LocalDateTime otherCreated = LocalDateTime.of(2008, 8, 8, 8, 8, 8);
    private static final String title = "Title";
    private static final String otherTitle = "Other title";
    private static final String content = "Content";
    private static final String otherContent = "Other content";
    private static final Message message = Message.create(created, title, content);

    @Test
    public void testGetCreated() {
        assertThat(message.getCreated(), is(created));
    }

    @Test
    public void testGetTitle() {
        assertThat(message.getTitle(), is(title));
    }

    @Test
    public void testGetContent() {
        assertThat(message.getContent(), is(content));
    }

    @Test
    public void testCreatedProperty() {
        assertThat(message.createdProperty().get(), is(created));
    }

    @Test
    public void testTitleProperty() {
        assertThat(message.titleProperty().get(), is(title));
    }

    @Test
    public void testContentProperty() {
        assertThat(message.contentProperty().get(), is(content));
    }

    @Test
    public void testEqualsWithSameMessageInstance() {
        assertThat(message, is(message));
    }

    @Test
    public void testEqualsWithOtherMessageInstance() {
        Message message2 = Message.create(created, title, content);
        assertThat(message2, is(message));
    }

    @Test
    public void testEqualsWithOtherCreated() {
        Message message2 = Message.create(otherCreated, title, content);
        assertThat(message2, not(message));
    }

    @Test
    public void testEqualsWithOtherTitle() {
        Message message2 = Message.create(created, otherTitle, content);
        assertThat(message2, not(message));
    }

    @Test
    public void testEqualsWithOtherContent() {
        Message message2 = Message.create(created, title, otherContent);
        assertThat(message2, not(message));
    }

    @Test
    public void testJAXBMarshaling() throws Exception {
        JAXBContext context = JAXBContext.newInstance(message.getClass());
        Marshaller marshaller = context.createMarshaller();
        Unmarshaller unmarshaller = context.createUnmarshaller();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        marshaller.marshal(message, outputStream);

        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        Message actualMessage = (Message) unmarshaller.unmarshal(inputStream);

        assertThat(actualMessage, is(message));
    }

}
