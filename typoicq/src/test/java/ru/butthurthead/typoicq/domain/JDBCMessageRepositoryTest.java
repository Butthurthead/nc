package ru.butthurthead.typoicq.domain;

import org.junit.Before;
import org.junit.Test;
import ru.butthurthead.typoicq.migration.InitialMigration;
import ru.butthurthead.typoicq.migration.Migration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class JDBCMessageRepositoryTest {

    private static final String jdbcDriverString = "org.sqlite.JDBC";
    private static final String connectionString = "jdbc:sqlite:";
    private static final List<Message> messages = Arrays.asList(
            Message.create(LocalDateTime.of(2007, 7, 7, 7, 7, 10), "Title 1", "Content 1"),
            Message.create(LocalDateTime.of(2007, 7, 8, 7, 8, 12), "Title 2", "Content 2"),
            Message.create(LocalDateTime.of(2007, 7, 9, 7, 9, 14), "Title 3", "Content 3")
    );
    private MessageRepository repository;

    private Connection connection;

    @Before
    public void setUp() throws Exception {
        Class.forName(jdbcDriverString);

        connection = DriverManager.getConnection(connectionString);

        Migration migration = new InitialMigration(connection);
        migration.up();

        repository = new JDBCMessageRepository(connection);

        for (Message message : messages) {
            repository.save(message);
        }
    }

    @Test
    public void hasMessages() throws Exception {
        List<Message> messagesFromRepository = repository.getAll();
        assertThat(messagesFromRepository, is(messages));
    }

}
