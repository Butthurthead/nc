package ru.butthurthead.typoicq.network;

import javafx.application.Application;
import org.junit.*;
import org.junit.rules.TemporaryFolder;
import ru.butthurthead.typoicq.domain.Message;
import ru.butthurthead.typoicq.util.JaxbParser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ClientTest {

    private static final String SERVER_HOST = "localhost";
    private static final int SERVER_PORT = 9000;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private Client client;
    private List<Message> messages;
    private Message messageToSend = Message.create(LocalDateTime.of(2007, 7, 7, 7, 7), "Title", "Content");

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Before
    public void setUp() throws Exception {
        messages = new LinkedList<>();

        Thread server = new Thread(() -> {
            try {
                serverSocket = new ServerSocket(SERVER_PORT);
                clientSocket = serverSocket.accept();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        server.start();

        client = new Client(c -> {
            while (c.next()) {
                messages.addAll(c.getAddedSubList());
            }
        });

        client.connect(SERVER_HOST, SERVER_PORT);

        // wait for connection
        while (clientSocket == null || !clientSocket.isConnected()) ;
    }

    @After
    public void tearDown() throws Exception {
        clientSocket.close();
        serverSocket.close();
    }

    @Test(timeout = 5000)
    public void sendMessage() throws Exception {
        client.sendMessage(messageToSend);
        InputStream inputStream = clientSocket.getInputStream();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        while (inputStream.available() > 0) {
            int b = inputStream.read();
            if (b != 0) {
                outputStream.write(b);
            } else {
                break;
            }
        }
        Message message = (Message) JaxbParser.unmarshall(
                Message.class, new ByteArrayInputStream(outputStream.toByteArray()));
        assertThat(message, is(messageToSend));
    }

    @Ignore("Test is broken because of javafx")
    @Test(timeout = 5000)
    public void retrieveMessage() throws Exception {
        Thread clientThread = new Thread(client);
        clientThread.start();

        OutputStream outputStream = clientSocket.getOutputStream();
        JaxbParser.marshall(messageToSend, outputStream);
        outputStream.write(0);

        // wait for message
        while (messages.size() == 0) ;

        assertThat(messages.contains(messageToSend), is(true));
    }
}
