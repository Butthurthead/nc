package ru.butthurthead.typoicq.util;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class LocalDateTimeAdapterTest {

    private static LocalDateTime objectRepresentation = LocalDateTime.of(2007, 7, 7, 7, 7, 7);
    private static String stringRepresentation = "2007-07-07 07:07:07";

    private static LocalDateTimeAdapter adapter = new LocalDateTimeAdapter();

    @Test
    public void convertToString() throws Exception {
        String convertedString = adapter.marshal(objectRepresentation);
        assertThat(convertedString, is(stringRepresentation));
    }

    @Test
    public void convertToObject() throws Exception {
        LocalDateTime unmarshalled = adapter.unmarshal(stringRepresentation);
        assertThat(unmarshalled, is(objectRepresentation));
    }
}
