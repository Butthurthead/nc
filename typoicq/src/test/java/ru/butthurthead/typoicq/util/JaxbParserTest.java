package ru.butthurthead.typoicq.util;

import org.junit.Test;
import ru.butthurthead.typoicq.domain.Message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class JaxbParserTest {

    @Test
    public void marshallUnmarshall() throws Exception {
        Message message = Message.create(LocalDateTime.of(2007, 7, 7, 7, 7), "Title", "Content");

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        JaxbParser.marshall(message, os);

        Message message2 = (Message) JaxbParser.unmarshall(Message.class, new ByteArrayInputStream(os.toByteArray()));
        assertThat(message2, is(message));
    }
}
