package ru.butthurthead.typoicq.util;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.nio.file.Files;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

public class FileEncryptorTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    private byte[] secretBytes = "Mary has a cat".getBytes();
    private String password = "qwerty1234";
    private byte[] encryptedBytes;
    private byte[] decryptedBytes;

    @Before
    public void setUp() throws Exception {
        File encryptedFile = folder.newFile();
        File decryptedFile = folder.newFile();

        Files.write(decryptedFile.toPath(), secretBytes);

        // encrypt file
        FileEncryptor.encrypt(password, decryptedFile, encryptedFile);
        encryptedBytes = Files.readAllBytes(encryptedFile.toPath());

        // decrypt file
        FileEncryptor.decrypt(password, encryptedFile, decryptedFile);
        decryptedBytes = Files.readAllBytes(decryptedFile.toPath());
    }

    @Test
    public void testEncryption() throws Exception {
        assertThat(encryptedBytes, not(secretBytes));
    }

    @Test
    public void testDecryption() throws Exception {
        assertThat(decryptedBytes, is(secretBytes));
    }
}
