package com.ncedu.mindvr.auction.common.protocol;

/**
 * Created by mindvr on 16.12.2015.
 */
public enum StatusCode {
    OK, BAD_REQUEST, SERVER_ERROR
}
