package com.ncedu.mindvr.auction.common.protocol;

/**
 * Created by mindvr on 16.12.2015.
 */
public enum MessageField {
    ACTION, NAME, PASSWORD, NEW_PASSWORD, LOT_ID,
    STATUS, EXCEPTION, ANSWER, LOTS, LOT, BID
}
