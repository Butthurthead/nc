package com.ncedu.mindvr.auction.common.protocol;

/**
 * Created by mindvr on 16.12.2015.
 */
public enum  ActionCode {
    CHECK_PASSWORD, REGISTER, CHANGE_PASSWORD, GET_LOTS, GET_LOT, PLACE_LOT,
    RETRACT_LOT, PLACE_BID, RETRACT_BID
}
