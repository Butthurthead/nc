package com.ncedu.mindvr.auction.common.protocol;

/**
 * Created by mindvr on 16.12.2015.
 */
public enum AnswerCode {
    NOT_REGISTERED ("User is not registered"),
    ALREADY_EXISTS ("User already exists"),
    REGISTERED ("New user registered"),
    WRONG_PASSWORD ("Wrong password"),
    AUTH_OK ("Logged in"),
    PASSWORD_CHANGED  ("Password changed"),
    EXCEPTION_THROWN ("Auction exception was thrown");

    private final String description;

    AnswerCode(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.description;
    }
}
