package com.ncedu.mindvr.auction.common.utils;

import com.sun.org.apache.xerces.internal.dom.DeferredTextImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * XML settings loader class.
 * Provides interface for string-string pairs in given xml file.
 */
public class Settings {

    /**
     * Map of key:value string pairs.
     */
    private Map<String, String> settings;
    /**
     * Path for settings file.
     */
    private String path;


    /**
     * Default constructor.
     * Note that load() must be called before usage of settings instance.
     *
     * @param path path to settings xml.
     */
    public Settings(String path) {
        this.path = path;
        this.settings = new HashMap<>();
    }

    /**
     * Loads settings map from given file.
     *
     * @param path String with settings xml file
     */
    public void load(String path) {
        try {
            if (validate(path)) {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(new File(path));
                doc.getDocumentElement().normalize();
                NodeList nList = doc.getDocumentElement().getChildNodes();
                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);
                    if (!(nNode instanceof DeferredTextImpl)) {
                        Element eElement = (Element) nNode;
                        String name = eElement.getAttribute("key");
                        String param = eElement.getAttribute("value");
                        this.settings.put(name, param);
                    }
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Validates given file with settings.xsd resource (this package)
     *
     * @param xmlPath String - path to file
     * @return true if xml document is valid
     */
    public boolean validate(String xmlPath) {
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(Settings.class.getResource("settings.xsd"));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlPath)));
        } catch (SAXException e) {
            System.out.println("Configuration file is not valid.");
            return false;
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    /**
     * Loads settings map from default location (specified in constructor)
     */
    public void load() {
        this.load(this.path);
    }

    /**
     * Saves settings map to specified file.
     *
     * @param path settings xml file path.
     */
    public void save(String path) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            Element rootElement = doc.createElement("settings");
            doc.appendChild(rootElement);
            for (Map.Entry<String, String> elem : this.settings.entrySet()) {
                Element settingsElement = doc.createElement("option");
                settingsElement.setAttribute("key", elem.getKey());
                settingsElement.setAttribute("value", elem.getValue());
                rootElement.appendChild(settingsElement);
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(path));
            transformer.transform(source, result);
        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves settings to default location.
     */
    public void save() {
        this.save(this.path);
    }

    /**
     * Returns value for specified key.
     *
     * @param key String with key name.
     * @return String with value corresponding to key.
     */
    public String getValue(String key) {
        return this.settings.get(key);
    }

    /**
     * Sets specified key:value pair
     *
     * @param key   String
     * @param value String
     */
    public void setValue(String key, String value) {
        if (this.settings.containsKey(key)) {
            this.settings.remove(key);
        }
        this.settings.put(key, value);
        save();
    }

}
