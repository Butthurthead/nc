package com.ncedu.mindvr.auction.common.model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.InputMismatchException;

/**
 * CompactLot is a stripped version of Lot used for client->server data exchange.
 * It has same fields as bigger class without server-specific ones.
 * Fields excluded:
 * id
 * sellerName
 * finished
 * topBid
 * topBidder
 * bids
 * Created by mindvr on 24.11.2015.
 */
public final class CompactLot implements Serializable {
    /**
     * Title of the lot
     */
    private String title;

    /**
     * Lot description, probably html/xml.
     */
    private String description;

    /**
     * Starting price of the lot.
     */
    private double minPrice;

    /**
     * Buyout price of the lot.
     */
    private double maxPrice;

    /**
     * Minimal increment to current highest bid
     */
    private double minIncrement;

    /**
     * Bidding period's end
     */
    private ZonedDateTime auctionEnd;

    /**
     * Snipe protection period's duration in seconds.
     * If someone places new bid in snipeDuration seconds before this.auctionEnd,
     * auctionEnd is extended by snipeIncrement seconds.
     */
    private int snipeDuration;

    /**
     * Snipe protection period's increment in seconds.
     * If someone places new bid in snipeDuration seconds before this.auctionEnd,
     * auctionEnd is extended by snipeIncrement seconds.
     */
    private int snipeIncrement;

    /**
     * Default constructor with input validation.
     *
     * @param title          lot title
     * @param description    detailed description
     * @param minPrice       auction starting price
     * @param maxPrice       buyout price
     * @param minIncrement   minimal bid increment
     * @param auctionEnd     auction's end time
     * @param snipeDuration  duration of snipe protection period in seconds
     * @param snipeIncrement period auction get's extended in case of snipe attempt
     * @throws NullPointerException
     * @throws InputMismatchException
     */
    public CompactLot(String title, String description, double minPrice, double maxPrice, double minIncrement,
                      ZonedDateTime auctionEnd, int snipeDuration, int snipeIncrement)
            throws NullPointerException, InputMismatchException {
        if (title == null ||
                description == null ||
                auctionEnd == null
                ) {
            throw new NullPointerException();
        } else if (minPrice > maxPrice ||
                minIncrement < 0 ||
                snipeDuration < 0 ||
                snipeIncrement < 0) {
            throw new InputMismatchException();
        }
        this.title = title;
        this.description = description;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.minIncrement = minIncrement;
        this.auctionEnd = auctionEnd;
        this.snipeDuration = snipeDuration;
        this.snipeIncrement = snipeIncrement;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public double getMinIncrement() {
        return minIncrement;
    }

    public ZonedDateTime getAuctionEnd() {
        return auctionEnd;
    }

    public int getSnipeDuration() {
        return snipeDuration;
    }

    public int getSnipeIncrement() {
        return snipeIncrement;
    }
}
