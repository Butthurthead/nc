package com.ncedu.mindvr.auction.common.model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Class represents a lot at server.
 * Bid related methods check correctness of actions, like finished/snipe protection states.
 */
public final class ServerLot implements Serializable {
    /**
     * Unique lot id.
     */
    private int id;

    /**
     * (User)name of the seller.
     */
    private String sellerName;

    /**
     * Title of the lot
     */
    private String title;

    /**
     * Lot description, probably html/xml.
     */
    private String description;

    /**
     * Starting price of the lot.
     */
    private double minPrice;

    /**
     * Buyout price of the lot.
     */
    private double maxPrice;

    /**
     * Minimal increment to current highest bid
     */
    private double minIncrement;

    /**
     * Bidding period's end
     */
    private ZonedDateTime auctionEnd;

    /**
     * Snipe protection period's duration in seconds.
     * If someone places new bid in snipeDuration seconds before this.auctionEnd,
     * auctionEnd is extended by snipeIncrement seconds.
     */
    private int snipeDuration;

    /**
     * Snipe protection period's increment in seconds.
     * If someone places new bid in snipeDuration seconds before this.auctionEnd,
     * auctionEnd is extended by snipeIncrement seconds.
     */
    private int snipeIncrement;

    /**
     * State of the lot.
     * New bids cannot be placed when finished = true.
     */
    private boolean finished = false;

    /**
     * Current highest bid.
     * Auxiliary field used as a workaround to lack of two way SortedMap.
     */
    private double topBid;

    /**
     * Current highest bidder.
     * Auxiliary field used as a workaround to lack of two way SortedMap.
     */
    private String topBidder;

    /**
     * Map stores name:bid pairs.
     * Only one bid per bidder is stored.
     */
    private Map<String, Double> bids;

    /**
     * Constructor with full set of fields.
     * Used for DB deserialization.
     *
     * @param id             lot id
     * @param sellerName     username of the seller
     * @param title          lot title
     * @param description    detailed description
     * @param minPrice       auction starting price
     * @param maxPrice       buyout price
     * @param minIncrement   minimal bid increment
     * @param auctionEnd     auction's end time
     * @param snipeDuration  duration of snipe protection period in seconds
     * @param snipeIncrement period auction get's extended in case of snipe attempt
     * @param finished       true if finished, else otherwise
     * @param bids           String:Double map of bids
     * @param topBid         top bid
     * @param topBidder      top bidder's name
     */
    public ServerLot(int id, String sellerName, String title, String description, double minPrice, double maxPrice,
                     double minIncrement, ZonedDateTime auctionEnd, int snipeDuration, int snipeIncrement, boolean finished,
                     Map<String, Double> bids, double topBid, String topBidder) {
        this.id = id;
        this.sellerName = sellerName;
        this.title = title;
        this.description = description;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.minIncrement = minIncrement;
        this.auctionEnd = auctionEnd;
        this.snipeDuration = snipeDuration;
        this.snipeIncrement = snipeIncrement;
        this.finished = finished;
        this.bids = bids;
        this.topBid = topBid;
        this.topBidder = topBidder;
    }

    /**
     * Server side constructor.
     * Creates a copy of provided lot
     *
     * @param id  lot id
     * @param lot CompactLot provided by client
     */
    public ServerLot(int id, String sellerName, CompactLot lot) {
        this.id = id;
        this.sellerName = sellerName;
        this.title = lot.getTitle();
        this.description = lot.getDescription();
        this.minPrice = lot.getMinPrice();
        this.maxPrice = lot.getMaxPrice();
        this.minIncrement = lot.getMinIncrement();
        this.auctionEnd = lot.getAuctionEnd();
        this.snipeDuration = lot.getSnipeDuration();
        this.snipeIncrement = lot.getSnipeIncrement();
        this.bids = new HashMap<>();
    }

    public int getId() {
        return id;
    }

    public String getSellerName() {
        return sellerName;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public double getMinIncrement() {
        return minIncrement;
    }

    public ZonedDateTime getAuctionEnd() {
        return auctionEnd;
    }

    public int getSnipeDuration() {
        return snipeDuration;
    }

    public int getSnipeIncrement() {
        return snipeIncrement;
    }

    public double getTopBid() {
        return topBid;
    }

    public String getTopBidder() {
        return topBidder;
    }

    public Map<String, Double> getBids() {
        return bids;
    }

    @Override
    public String toString() {
        return "Lot{" +
                "id=" + id +
                ", sellerName='" + sellerName + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", minPrice=" + minPrice +
                ", maxPrice=" + maxPrice +
                ", minIncrement=" + minIncrement +
                ", auctionEnd=" + auctionEnd +
                ", snipeDuration=" + snipeDuration +
                ", snipeIncrement=" + snipeIncrement +
                ", finished=" + finished +
                ", topBid=" + topBid +
                ", topBidder='" + topBidder + '\'' +
                ", bids=" + bids +
                '}';
    }

    /**
     * Updates finished status.
     *
     * @return true if auction has finished, false otherwise.
     */
    public boolean isFinished() {
        this.finished = ZonedDateTime.now().isAfter(this.auctionEnd);
        return this.finished;
    }

    /**
     * Places bid on auction.
     * Bid should be valid (lot duration, min increment, etc). Method also updates top bid/bidder fields.
     *
     * @param name bidder's name
     * @param bid  Double bid
     * @throws AuctionException if bid doesn't qualify the rules
     */
    public synchronized void placeBid(String name, double bid) throws AuctionException {
        if (this.isFinished()) {
            throw new AuctionException("auction is over");
        } else {
            if (this.bids.isEmpty()) {
                //First bid
                if (bid < minPrice) {
                    //The bid is too low
                    throw new AuctionException("invalid bid");
                } else if (bid >= this.maxPrice) {
                    //The bid matches buyout
                    this.topBid = this.maxPrice;
                    this.topBidder = name;
                    this.bids.put(name, maxPrice);
                    this.finished = true;
                } else {
                    //The bid is between min and BO
                    this.topBid = bid;
                    this.topBidder = name;
                    this.bids.put(name, bid);
                    if (this.isProtected()) {
                        this.auctionEnd = this.auctionEnd.plusSeconds(this.snipeIncrement);
                    }
                }
            } else {
                //Not the first bid
                if (bid < (this.topBid + this.minIncrement)) {
                    //The bid is too low
                    throw new AuctionException("invalid bid");
                } else if (bid >= this.maxPrice) {
                    //The bid matches buyout
                    this.topBid = this.maxPrice;
                    this.topBidder = name;
                    this.bids.put(name, maxPrice);
                    this.finished = true;
                } else {
                    //The bid is between min and BO
                    this.topBid = bid;
                    this.topBidder = name;
                    if (this.bids.containsKey(name)) {
                        this.bids.remove(name);
                        this.bids.put(name, bid);
                        if (this.isProtected()) {
                            this.auctionEnd = this.auctionEnd.plusSeconds(this.snipeIncrement);
                        }
                    } else {
                        this.bids.put(name, bid);
                        if (this.isProtected()) {
                            this.auctionEnd = this.auctionEnd.plusSeconds(this.snipeIncrement);
                        }
                    }
                }
            }
        }
    }

    /**
     * Retracts particular bid.
     * If the bid being retracted is the top one - top bid/bidders fields will be recalculated.
     *
     * @param name bidder's name
     * @throws AuctionException if auction is over or there are no bids from specified bidder.
     */
    public synchronized void retractBid(String name) throws AuctionException {
        if (this.isFinished()) {
            //No bid retracting on finished bids
            throw new AuctionException("auction is over");
        } else {
            if (!this.topBidder.equals(name)) {
                //Retracted bid isn't the top one.
                if (this.bids.containsKey(name)) {
                    this.bids.remove(name);
                } else {
                    throw new AuctionException("No such bid");
                }
            } else {
                //Retracted bid is the top one.
                this.bids.remove(name);
                if (this.bids.isEmpty()) {
                    this.topBidder = null;
                    this.topBid = 0;
                } else {
                    //Searching for new top bid;
                    double newTopBid = 0;
                    String newTopBidder = ""; //Workaround, there is at least one bid since this.bids isn't empty;
                    for (Map.Entry<String, Double> entry : this.bids.entrySet()) {
                        if (entry.getValue() > newTopBid) {
                            newTopBid = entry.getValue();
                            newTopBidder = entry.getKey();
                        }
                    }
                    this.topBid = newTopBid;
                    this.topBidder = newTopBidder;
                }
            }
        }
    }

    /**
     * Checks if lot is snipe protection stage.
     *
     * @return true if snipe protected, false otherwise.
     */
    private boolean isProtected() {
        return (ZonedDateTime.now().isAfter(this.auctionEnd.minusSeconds(this.snipeDuration)));
    }

}
