package com.ncedu.mindvr.auction.common.model;

import java.io.Serializable;

/**
 * Utility exception class for handling incorrect input for Lot class.
 */
public class AuctionException extends Exception implements Serializable {

    public AuctionException(String message) {
        super(message);
    }

}
