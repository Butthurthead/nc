package com.ncedu.mindvr.auction.client.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Gui representation of bid for bid table.
 */
public class Bid {
    private final SimpleStringProperty bidderName;
    private final SimpleDoubleProperty bid;

    public Bid(String name, Double bid){
        this.bidderName = new SimpleStringProperty(name);
        this.bid = new SimpleDoubleProperty(bid);
    }

//    public String getBidderName(){
//        return this.bidderName.get();
//    }

    public StringProperty bidderNameProperty(){
        return this.bidderName;
    }

//    public void getBidderName(String name){
//        this.bidderName.set(name);
//    }

    public Double getBid(){
        return this.bid.get();
    }

    public DoubleProperty bidProperty(){
        return this.bid;
    }

    public void setBid(Double bid){
        this.bid.set(bid);
    }
}
