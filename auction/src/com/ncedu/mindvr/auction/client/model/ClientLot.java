package com.ncedu.mindvr.auction.client.model;

import com.ncedu.mindvr.auction.common.model.ServerLot;
import javafx.beans.property.*;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Client representation of lot for lots table;
 */
public class ClientLot {
    private final SimpleIntegerProperty id;
    private final SimpleStringProperty sellerName;
    private final SimpleStringProperty title;
    private final SimpleStringProperty description;
    private final SimpleDoubleProperty minPrice;
    private final SimpleDoubleProperty maxPrice;
    private final SimpleDoubleProperty minIncrement;
    private final SimpleObjectProperty<ZonedDateTime> auctionEnd;
    private final SimpleIntegerProperty snipeDuration;
    private final SimpleIntegerProperty snipeIncrement;
    private final SimpleBooleanProperty finished;
    private final SimpleDoubleProperty topBid;
    private final SimpleStringProperty topBidder;
    private List<Bid> bids;

    public ClientLot(ServerLot serverLot) {
        id = new SimpleIntegerProperty(serverLot.getId());
        sellerName = new SimpleStringProperty(serverLot.getSellerName());
        title = new SimpleStringProperty(serverLot.getTitle());
        description = new SimpleStringProperty(serverLot.getDescription());
        minPrice = new SimpleDoubleProperty(serverLot.getMinPrice());
        maxPrice = new SimpleDoubleProperty(serverLot.getMaxPrice());
        minIncrement = new SimpleDoubleProperty(serverLot.getMinIncrement());
        auctionEnd = new SimpleObjectProperty<>(serverLot.getAuctionEnd());
        snipeDuration = new SimpleIntegerProperty(serverLot.getSnipeDuration());
        snipeIncrement = new SimpleIntegerProperty(serverLot.getSnipeIncrement());
        finished = new SimpleBooleanProperty(serverLot.isFinished());
        topBid = new SimpleDoubleProperty(serverLot.getTopBid());
        topBidder = new SimpleStringProperty(serverLot.getTopBidder());
        bids = new ArrayList<>();
        for (Map.Entry<String, Double> e : serverLot.getBids().entrySet()) {
            bids.add(new Bid(e.getKey(), e.getValue()));
        }
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

//    public String getSellerName() {
//        return sellerName.get();
//    }

    public SimpleStringProperty sellerNameProperty() {
        return sellerName;
    }

//    public String getTitle() {
//        return title.get();
//    }

    public SimpleStringProperty titleProperty() {
        return title;
    }

//    public String getDescription() {
//        return description.get();
//    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

//    public double getMinPrice() {
//        return minPrice.get();
//    }

    public SimpleDoubleProperty minPriceProperty() {
        return minPrice;
    }

//    public double getMaxPrice() {
//        return maxPrice.get();
//    }

    public SimpleDoubleProperty maxPriceProperty() {
        return maxPrice;
    }

//    public double getMinIncrement() {
//        return minIncrement.get();
//    }

    public SimpleDoubleProperty minIncrementProperty() {
        return minIncrement;
    }

//    public ZonedDateTime getAuctionEnd() {
//        return auctionEnd.get();
//    }

    public SimpleObjectProperty<ZonedDateTime> auctionEndProperty() {
        return auctionEnd;
    }

//    public int getSnipeDuration() {
//        return snipeDuration.get();
//    }

    public SimpleIntegerProperty snipeDurationProperty() {
        return snipeDuration;
    }

//    public int getSnipeIncrement() {
//        return snipeIncrement.get();
//    }

    public SimpleIntegerProperty snipeIncrementProperty() {
        return snipeIncrement;
    }

//    public boolean getFinished() {
//        return finished.get();
//    }

    public SimpleBooleanProperty finishedProperty() {
        return finished;
    }

//    public double getTopBid() {
//        return topBid.get();
//    }

    public SimpleDoubleProperty topBidProperty() {
        return topBid;
    }

//    public String getTopBidder() {
//        return topBidder.get();
//    }

    public SimpleStringProperty topBidderProperty() {
        return topBidder;
    }

    public List<Bid> getBids() {
        return bids;
    }

    @Override
    public String toString() {
        return "ClientLot{" +
                "id=" + id +
                ", sellerName=" + sellerName +
                ", title=" + title +
                ", topBid=" + topBid +
                ", topBidder=" + topBidder +
                ", bids=" + bids +
                '}';
    }
}
