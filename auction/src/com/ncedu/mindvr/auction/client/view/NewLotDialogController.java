package com.ncedu.mindvr.auction.client.view;

import com.ncedu.mindvr.auction.common.model.CompactLot;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.time.ZonedDateTime;
import java.util.InputMismatchException;


public class NewLotDialogController {
    @FXML
    private TextField titleBox;
    @FXML
    private TextField descriptionBox;
    @FXML
    private TextField minPriceBox;
    @FXML
    private TextField maxPriceBox;
    @FXML
    private TextField minIncrementBox;
    @FXML
    private TextField endsInBox;
    @FXML
    private TextField snipeDurationBox;
    @FXML
    private TextField snipeIncrementBox;
    @FXML
    private Label statusLabel;

    private Stage dialogStage;
    private CompactLot compactLot;
    private boolean okClicked = false;

    public CompactLot getCompactLot() {
        return compactLot;
    }

    @FXML
    private void initialize() {

    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public boolean isOkClicked() {
        return this.okClicked;
    }

    @FXML
    private void handleOk() {
        try {
            this.compactLot = new CompactLot(
                    titleBox.getText(),
                    descriptionBox.getText(),
                    Double.valueOf(minPriceBox.getText()),
                    Double.valueOf(maxPriceBox.getText()),
                    Double.valueOf(minIncrementBox.getText()),
                    ZonedDateTime.now().plusMinutes(Integer.valueOf(endsInBox.getText())),
                    Integer.valueOf(snipeDurationBox.getText()),
                    Integer.valueOf(snipeIncrementBox.getText())
            );
            okClicked = true;
            dialogStage.close();
        } catch (NullPointerException | InputMismatchException | NumberFormatException e) {
            statusLabel.setText("Input mismatch");
        }
    }

    @FXML
    private void handleCancel() {
        this.dialogStage.close();
    }
}
