package com.ncedu.mindvr.auction.client.view;

import com.ncedu.mindvr.auction.client.ClientFX;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class LoginDialogController {
    @FXML
    private PasswordField passwordField;
    @FXML
    private TextField loginField;
    @FXML
    private Label statusLabel;
    @FXML
    private Button loginButton;
    @FXML
    private Button registerButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button cancelButton;

    private ClientFX mainApp;
    private Stage dialogStage;

    public void setCallbacks(ClientFX mainApp, Stage dialogStage) {
        this.mainApp = mainApp;
        this.dialogStage = dialogStage;
        this.loginField.setText(mainApp.getServerWrapper().getLogin());
        this.passwordField.setText(mainApp.getServerWrapper().getPassword());
    }

    public LoginDialogController() {
    }

    @FXML
    private void initialize() {
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    @FXML
    private void handleLogin() {
        statusLabel.setText(mainApp.getServerWrapper().tryAuth(loginField.getText(),
                passwordField.getText()));
    }

    @FXML
    private void handleRegister() {
        statusLabel.setText(mainApp.getServerWrapper().register(loginField.getText(),
                passwordField.getText()));
    }

    @FXML
    private void handleSave() {
        statusLabel.setText(mainApp.getServerWrapper().savePassword(loginField.getText(),
                passwordField.getText()));
    }

}
