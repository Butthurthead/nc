package com.ncedu.mindvr.auction.client.view;

import com.ncedu.mindvr.auction.client.ClientFX;
import com.ncedu.mindvr.auction.client.model.Bid;
import com.ncedu.mindvr.auction.client.model.ClientLot;
import com.ncedu.mindvr.auction.common.model.ServerLot;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import java.time.ZonedDateTime;

/**
 * View-controller of main (auction overview window)
 */
public class AuctionOverviewController {
    @FXML
    private TableView<ClientLot> lotsTable;
    @FXML
    private TableColumn<ClientLot, Integer> idColumn;
    @FXML
    private TableColumn<ClientLot, String> sellerColumn;
    @FXML
    private TableColumn<ClientLot, String> titleColumn;
    @FXML
    private TableColumn<ClientLot, String> descriptionColumn;
    @FXML
    private TableColumn<ClientLot, Double> minPriceColumn;
    @FXML
    private TableColumn<ClientLot, Double> maxPriceColumn;
    @FXML
    private TableColumn<ClientLot, Double> incrementColumn;
    @FXML
    private TableColumn<ClientLot, ZonedDateTime> endsColumn;
    @FXML
    private TableColumn<ClientLot, Integer> snipeDurationColumn;
    @FXML
    private TableColumn<ClientLot, Integer> snipeIncrementColumn;
    @FXML
    private TableColumn<ClientLot, Boolean> finishedColumn;
    @FXML
    private TableColumn<ClientLot, Double> topBidColumn;
    @FXML
    private TableColumn<ClientLot, String> topBidderColumn;


    @FXML
    private TableView<Bid> bidsTable;
    @FXML
    private TableColumn<Bid, String> bidderColumn;
    @FXML
    private TableColumn<Bid, Double> bidColumn;

    @FXML
    private Label statusLabel;
    @FXML
    private Label loginStatusLabel;
    @FXML
    private TextField bidField;

    private ClientFX mainApp;

    public void setMainApp(ClientFX mainApp) {
        this.mainApp = mainApp;
        lotsTable.setItems(mainApp.getLotsData());
        bidsTable.setItems(mainApp.getBidsData());
        loginStatusLabel.setText(mainApp.getServerWrapper().tryAuth());
    }


    public AuctionOverviewController() {
    }


    @FXML
    private void initialize() {
        //Lots table
        this.idColumn.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        this.sellerColumn.setCellValueFactory(cellData -> cellData.getValue().sellerNameProperty());
        this.titleColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
        this.descriptionColumn.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
        this.minPriceColumn.setCellValueFactory(cellData -> cellData.getValue().minPriceProperty().asObject());
        this.maxPriceColumn.setCellValueFactory(cellData -> cellData.getValue().maxPriceProperty().asObject());
        this.incrementColumn.setCellValueFactory(cellData -> cellData.getValue().minIncrementProperty().asObject());
        this.endsColumn.setCellValueFactory(cellData -> cellData.getValue().auctionEndProperty());
        this.snipeDurationColumn.setCellValueFactory(cellData -> cellData.getValue().snipeDurationProperty().asObject());
        this.snipeIncrementColumn.setCellValueFactory(cellData -> cellData.getValue().snipeIncrementProperty().asObject());
        this.finishedColumn.setCellValueFactory(cellData -> cellData.getValue().finishedProperty().asObject());
        this.topBidColumn.setCellValueFactory(cellData -> cellData.getValue().topBidProperty().asObject());
        this.topBidderColumn.setCellValueFactory(cellData -> cellData.getValue().topBidderProperty());
        //Bids table
        this.bidderColumn.setCellValueFactory(cellData -> cellData.getValue().bidderNameProperty());
        this.bidColumn.setCellValueFactory(cellData -> cellData.getValue().bidProperty().asObject());
        //Lots table selection handler
        this.lotsTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showBids(newValue));
    }

    private void showBids(ClientLot lot) {
        this.mainApp.getBidsData().clear();
        if (lot != null) {
            this.mainApp.getBidsData().addAll(lot.getBids());
        }
    }

    @FXML
    private void refreshLots() {
        int selectedIndex = lotsTable.getSelectionModel().getSelectedIndex();
        mainApp.getLotsData().clear();
        for (ServerLot lot : mainApp.getServerWrapper().getLots()) {
            mainApp.getLotsData().add(new ClientLot(lot));
        }
        lotsTable.getSelectionModel().select(selectedIndex);
    }

    @FXML
    private void openLoginDialog() {
        mainApp.showLoginDialog();
    }

    @FXML
    private void handleNewLot() {
        statusLabel.setText(mainApp.showNewLotDialog());
        refreshLots();
    }

    @FXML
    private void handlePlaceBid() {
        try {
            Double bid = Double.valueOf(bidField.getText());
            int selectedIndex = lotsTable.getSelectionModel().getSelectedIndex();
            if (selectedIndex < 0) {
                statusLabel.setText("Nothing is selected");
                return;
            }
            int lotId = lotsTable.getItems().get(selectedIndex).getId();
            statusLabel.setText(mainApp.getServerWrapper().placeBid(lotId, bid));
            refreshLots();
        } catch (NumberFormatException e) {
            statusLabel.setText("Enter valid bid");
        }
    }
}
