package com.ncedu.mindvr.auction.client;

import com.ncedu.mindvr.auction.client.controller.ServerWrapper;
import com.ncedu.mindvr.auction.client.model.Bid;
import com.ncedu.mindvr.auction.client.model.ClientLot;
import com.ncedu.mindvr.auction.client.view.AuctionOverviewController;
import com.ncedu.mindvr.auction.client.view.LoginDialogController;
import com.ncedu.mindvr.auction.client.view.NewLotDialogController;
import com.ncedu.mindvr.auction.common.utils.Settings;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class ClientFX extends Application {
    /**
     * Container for lots table
     */
    private ObservableList<ClientLot> lotsData = FXCollections.observableArrayList();
    /**
     * Container for bids table
     */
    private ObservableList<Bid> bidsData = FXCollections.observableArrayList();
    private Stage primaryStage;
    private ServerWrapper serverWrapper;

    public ClientFX() {
        Settings settings = new Settings("./ClientData/settings.xml");
        settings.load();
        this.serverWrapper = new ServerWrapper(settings);
    }

    public ObservableList<ClientLot> getLotsData() {
        return lotsData;
    }

    public ObservableList<Bid> getBidsData() {
        return bidsData;
    }

    public ServerWrapper getServerWrapper() {
        return serverWrapper;
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Auction");
        initAuctionOverview();
    }


    /**
     * Loads and shows main window.
     */
    public void initAuctionOverview() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClientFX.class.getResource("view/AuctionOverview.fxml"));
            AnchorPane auctionOverview = loader.load();
            AuctionOverviewController controller = loader.getController();
            controller.setMainApp(this);
            Scene scene = new Scene(auctionOverview);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void showLoginDialog() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClientFX.class.getResource("view/LoginDialog.fxml"));
            AnchorPane loginDialog = loader.load();
            //Creating the dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Login dialog");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.primaryStage);
            Scene scene = new Scene(loginDialog);
            dialogStage.setScene(scene);
            //Setting controller
            LoginDialogController controller = loader.getController();
            controller.setCallbacks(this, dialogStage);
            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows new lot dialog.
     *
     * @return string to be shown in status.
     */
    public String showNewLotDialog() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClientFX.class.getResource("view/NewLotDialog.fxml"));
            AnchorPane newLotDialog = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("New lot");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.primaryStage);
            Scene scene = new Scene(newLotDialog);
            dialogStage.setScene(scene);
            NewLotDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            dialogStage.showAndWait();
            if (controller.isOkClicked()) {
                return serverWrapper.placeLot(controller.getCompactLot());
            } else {
                return "";
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "err";
        }
    }
}
