package com.ncedu.mindvr.auction.client.controller;

import com.ncedu.mindvr.auction.common.model.CompactLot;
import com.ncedu.mindvr.auction.common.model.ServerLot;
import com.ncedu.mindvr.auction.common.protocol.ActionCode;
import com.ncedu.mindvr.auction.common.protocol.AnswerCode;
import com.ncedu.mindvr.auction.common.protocol.MessageField;
import com.ncedu.mindvr.auction.common.protocol.StatusCode;
import com.ncedu.mindvr.auction.common.utils.Settings;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Proxy for server {@link com.ncedu.mindvr.auction.server.controller.MessageManager} class.
 * Forms requests in Map<Field, Object> format and processes replies to required buy GUI form.
 */
public class ServerWrapper {
    private Settings settings;
    private int port;
    private InetAddress hostname;
    private String login;
    private String password;

    public ServerWrapper(Settings settings) {
        this.settings = settings;
        this.port = Integer.valueOf(settings.getValue("server-port"));
        try {
            this.hostname = InetAddress.getByName(settings.getValue("server-address"));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        this.login = settings.getValue("login");
        this.password = settings.getValue("password");
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    private Map<MessageField, Object> serverRequest(Map<MessageField, Object> request) {
        try {
            Socket soc = new Socket(this.hostname, this.port);
            ObjectOutput oOut = new ObjectOutputStream(soc.getOutputStream());
            ObjectInput oIn = new ObjectInputStream(soc.getInputStream());
            oOut.writeObject(request);
            //We have good faith in server
            @SuppressWarnings("unchecked")
            Map<MessageField, Object> reply = (Map<MessageField, Object>) oIn.readObject();
            return reply;
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    public Collection<ServerLot> getLots() {
        Map<MessageField, Object> request = new HashMap<>();
        request.put(MessageField.ACTION, ActionCode.GET_LOTS);
        Map<MessageField, Object> reply = this.serverRequest(request);
        if ((reply != null) && reply.get(MessageField.STATUS).equals(StatusCode.OK)) {
            @SuppressWarnings("unchecked")
            Collection<ServerLot> lots = ((Map) reply.get(MessageField.LOTS)).values();
            return lots;
        } else {
            return null;
        }
    }

    public String tryAuth(String login, String password) {
        Map<MessageField, Object> request = new HashMap<>();
        request.put(MessageField.ACTION, ActionCode.CHECK_PASSWORD);
        request.put(MessageField.NAME, login);
        request.put(MessageField.PASSWORD, password);
        Map<MessageField, Object> reply = this.serverRequest(request);
        return reply.get(MessageField.ANSWER).toString();
    }

    public String tryAuth() {
        return tryAuth(this.login, this.password)+" as "+ this.login;
    }

    public String register(String login, String password) {
        Map<MessageField, Object> request = new HashMap<>();
        request.put(MessageField.ACTION, ActionCode.REGISTER);
        request.put(MessageField.NAME, login);
        request.put(MessageField.PASSWORD, password);
        Map<MessageField, Object> reply = this.serverRequest(request);
        return reply.get(MessageField.ANSWER).toString();
    }

    public String savePassword(String login, String password) {
        this.login = login;
        this.password = password;
        settings.setValue("login", login);
        settings.setValue("password", password);
        settings.save();
        return "Saved";
    }

    public String placeLot(CompactLot lot) {
        Map<MessageField, Object> request = new HashMap<>();
        request.put(MessageField.ACTION, ActionCode.PLACE_LOT);
        request.put(MessageField.NAME, this.login);
        request.put(MessageField.PASSWORD, this.password);
        request.put(MessageField.LOT, lot);
        Map<MessageField, Object> reply = this.serverRequest(request);
        if (reply.get(MessageField.STATUS).equals(StatusCode.SERVER_ERROR)) {
            return reply.get(MessageField.ANSWER).toString();
        } else {
            return "New lot#" + ((ServerLot) reply.get(MessageField.LOT)).getId() + " created";
        }
    }

    public String placeBid(int lotId, double bid) {
        Map<MessageField, Object> request = new HashMap<>();
        request.put(MessageField.ACTION, ActionCode.PLACE_BID);
        request.put(MessageField.NAME, this.login);
        request.put(MessageField.PASSWORD, this.password);
        request.put(MessageField.LOT_ID, lotId);
        request.put(MessageField.BID, bid);
        Map<MessageField, Object> reply = this.serverRequest(request);
        if (reply.get(MessageField.STATUS).equals(StatusCode.OK)) {
            return "Bid " + lotId + ":" + bid + " placed";
        } else if (reply.get(MessageField.ANSWER).equals(AnswerCode.EXCEPTION_THROWN)) {
            Exception e = (Exception) reply.get(MessageField.EXCEPTION);
            return "Error: " + e.getMessage();
        } else {
            return "Error: " + reply.get(MessageField.ANSWER).toString();
        }
    }

    @Override
    public String toString() {
        return "ServerWrapper{" +
                "settings=" + settings +
                ", port=" + port +
                ", hostname=" + hostname +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
