package com.ncedu.mindvr.auction.server;

import com.ncedu.mindvr.auction.common.utils.Settings;
import com.ncedu.mindvr.auction.server.controller.DBManager;
import com.ncedu.mindvr.auction.server.controller.MessageManager;
import com.ncedu.mindvr.auction.server.controller.ObjectServer;
import com.ncedu.mindvr.auction.server.model.Auction;

public class ServerLauncher {
    public static void main(String[] args) {
        Settings settings = new Settings("./ServerData/settings.xml");
        settings.load();
        DBManager dbManager = new DBManager(settings);
        Auction auction = new Auction(dbManager);
        MessageManager messageManager = new MessageManager(dbManager, auction);
        ObjectServer objectServer = new ObjectServer(messageManager, settings);
        objectServer.listen();
    }
}
