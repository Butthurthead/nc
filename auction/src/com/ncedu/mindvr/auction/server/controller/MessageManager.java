package com.ncedu.mindvr.auction.server.controller;


import com.ncedu.mindvr.auction.common.model.AuctionException;
import com.ncedu.mindvr.auction.common.model.CompactLot;
import com.ncedu.mindvr.auction.common.model.ServerLot;
import com.ncedu.mindvr.auction.common.protocol.ActionCode;
import com.ncedu.mindvr.auction.common.protocol.AnswerCode;
import com.ncedu.mindvr.auction.common.protocol.MessageField;
import com.ncedu.mindvr.auction.common.protocol.StatusCode;
import com.ncedu.mindvr.auction.server.model.Auction;

import java.util.HashMap;
import java.util.Map;

/**
 * Handles requests from clients.
 * <p>
 * Validates incoming messages and passes calls down to {@link com.ncedu.mindvr.auction.server.model.Auction} and
 * {@link DBManager} methods.
 *
 * @see com.ncedu.mindvr.auction.common.protocol for enums reference
 */
public class MessageManager {
    private DBManager dbManager;
    private Auction auction;


    public MessageManager(DBManager dbManager, Auction auction) {
        this.dbManager = dbManager;
        this.auction = auction;
    }


    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    /**
     * Parses client's request.
     * Request is a MessageField:Object Map, every request has one obligatory field - MessageField.ACTION
     * and additional fields if action requeres these.
     * Message format for each action is listed in corresponding method.
     *
     * @param request MessageField:Object map to be parsed
     * @return MessageField:Object map for client
     */
    public Map<MessageField, Object> processRequest(Map<MessageField, Object> request) {
        Map<MessageField, Object> reply = new HashMap<>();
        if (request.containsKey(MessageField.ACTION)) {
            switch ((ActionCode) request.get(MessageField.ACTION)) {
                case CHECK_PASSWORD:
                    this.checkPassword(request, reply);
                    break;
                case REGISTER:
                    this.register(request, reply);
                    break;
                case CHANGE_PASSWORD:
                    this.changePassword(request, reply);
                    break;
                case GET_LOTS:
                    this.getLots(reply);
                    break;
                case GET_LOT:
                    this.getLot(request, reply);
                    break;
                case PLACE_LOT:
                    this.placeLot(request, reply);
                    break;
                case RETRACT_LOT:
                    this.retractLot(request, reply);
                    break;
                case PLACE_BID:
                    this.placeBid(request, reply);
                    break;
                case RETRACT_BID:
                    this.retractBid(request, reply);
                    break;
                default:
                    reply.put(MessageField.STATUS, StatusCode.BAD_REQUEST);
                    break;
            }
        } else {
            reply.put(MessageField.STATUS, StatusCode.BAD_REQUEST);
        }
        return reply;
    }

    /**
     * Forms reply in case of AuctionException.
     *
     * @param reply     standard reply map
     * @param exception AuctionException generated in Auction.
     */
    private void packException(Map<MessageField, Object> reply, AuctionException exception) {
        reply.put(MessageField.STATUS, StatusCode.SERVER_ERROR);
        reply.put(MessageField.ANSWER, AnswerCode.EXCEPTION_THROWN);
        reply.put(MessageField.EXCEPTION, exception);
    }

    /**
     * Utility method, checks if request has correct name and password fields
     *
     * @param request standard request map
     * @return true if name and password are present and strings
     */
    private boolean isAuthValid(Map<MessageField, Object> request) {
        return request.containsKey(MessageField.NAME) && request.get(MessageField.NAME) instanceof String &&
                request.containsKey(MessageField.PASSWORD) && request.get(MessageField.PASSWORD) instanceof String;
    }

    /**
     * Checks name:password pair.
     * Request fields:
     *
     * @param request map, fields: name, password
     * @param reply   standard reply map
     */
    private void checkPassword(Map<MessageField, Object> request, Map<MessageField, Object> reply) {
        if (isAuthValid(request)) {
            reply.put(MessageField.STATUS, StatusCode.OK);
            if (!this.dbManager.userExist((String) request.get(MessageField.NAME))) {
                reply.put(MessageField.ANSWER, AnswerCode.NOT_REGISTERED);
            } else if (!this.dbManager.checkAuth((String) request.get(MessageField.NAME),
                    (String) request.get(MessageField.PASSWORD))) {
                reply.put(MessageField.ANSWER, AnswerCode.WRONG_PASSWORD);
            } else {
                reply.put(MessageField.ANSWER, AnswerCode.AUTH_OK);
            }
        } else {
            reply.put(MessageField.STATUS, StatusCode.BAD_REQUEST);
        }
    }

    /**
     * Registers new user.
     *
     * @param request map, fields: name, password
     * @param reply   standard reply map
     */
    private void register(Map<MessageField, Object> request, Map<MessageField, Object> reply) {
        if (isAuthValid(request)) {
            if (this.dbManager.userExist((String) request.get(MessageField.NAME))) {
                reply.put(MessageField.STATUS, StatusCode.SERVER_ERROR);
                reply.put(MessageField.ANSWER, AnswerCode.ALREADY_EXISTS);
            } else {
                dbManager.registerUser((String) request.get(MessageField.NAME),
                        (String) request.get(MessageField.PASSWORD));
                reply.put(MessageField.STATUS, StatusCode.OK);
                reply.put(MessageField.ANSWER, AnswerCode.REGISTERED);
            }
        } else {
            reply.put(MessageField.STATUS, StatusCode.BAD_REQUEST);
        }
    }

    /**
     * Changes password of existing user.
     *
     * @param request map, fields: name, password, new password
     * @param reply   standard reply map
     */
    private void changePassword(Map<MessageField, Object> request, Map<MessageField, Object> reply) {
        if (isAuthValid(request) &&
                request.containsKey(MessageField.NEW_PASSWORD) &&
                request.get(MessageField.NEW_PASSWORD) instanceof String) {
            if (this.dbManager.checkAuth((String) request.get(MessageField.NAME),
                    (String) request.get(MessageField.PASSWORD))) {
                this.dbManager.updatePassword((String) request.get(MessageField.NAME),
                        (String) request.get(MessageField.NEW_PASSWORD));
                reply.put(MessageField.STATUS, StatusCode.OK);
                reply.put(MessageField.ANSWER, AnswerCode.PASSWORD_CHANGED);
            } else {
                reply.put(MessageField.STATUS, StatusCode.SERVER_ERROR);
                reply.put(MessageField.ANSWER, AnswerCode.WRONG_PASSWORD);
            }
        } else {
            reply.put(MessageField.STATUS, StatusCode.BAD_REQUEST);
        }
    }

    /**
     * Returns all lots in Map<Integer, ServerLot> form
     *
     * @param reply map
     */
    private void getLots(Map<MessageField, Object> reply) {
        reply.put(MessageField.STATUS, StatusCode.OK);
        reply.put(MessageField.LOTS, auction.getLots());
    }

    /**
     * Returns lot with given id.
     *
     * @param request map, field: lot_id
     * @param reply   map
     */
    private void getLot(Map<MessageField, Object> request, Map<MessageField, Object> reply) {
        if (request.containsKey(MessageField.LOT_ID) && request.get(MessageField.LOT_ID) instanceof Integer) {
            try {
                ServerLot lot = this.auction.getLot((Integer) request.get(MessageField.LOT_ID));
                reply.put(MessageField.STATUS, StatusCode.OK);
                reply.put(MessageField.LOT, lot);
            } catch (AuctionException e) {
                this.packException(reply, e);
            }
        } else {
            reply.put(MessageField.STATUS, StatusCode.BAD_REQUEST);
        }
    }

    /**
     * Adds  new lot to auction.
     * User must provide correct name:password pair.
     *
     * @param request map, fields: name, password, lot(CompactLot)
     * @param reply   map
     */
    private void placeLot(Map<MessageField, Object> request, Map<MessageField, Object> reply) {
        if (isAuthValid(request) &&
                request.containsKey(MessageField.LOT) &&
                request.get(MessageField.LOT) instanceof CompactLot) {
            if (this.dbManager.checkAuth((String) request.get(MessageField.NAME),
                    (String) request.get(MessageField.PASSWORD))) {
                reply.put(MessageField.STATUS, StatusCode.OK);
                reply.put(MessageField.LOT,
                        this.auction.placeLot((String) request.get(MessageField.NAME),
                                (CompactLot) request.get(MessageField.LOT)));
            } else {
                reply.put(MessageField.STATUS, StatusCode.SERVER_ERROR);
                reply.put(MessageField.ANSWER, AnswerCode.WRONG_PASSWORD);
            }
        } else {
            reply.put(MessageField.STATUS, StatusCode.BAD_REQUEST);
        }
    }

    /**
     * Deletes lot of the owner.
     * Will return error if login is invalid or client attempts to delete someone else's lot.
     *
     * @param request map, fields: name, password, lot_id
     * @param reply   map
     */
    private void retractLot(Map<MessageField, Object> request, Map<MessageField, Object> reply) {
        if (this.isAuthValid(request) &&
                request.containsKey(MessageField.LOT_ID) &&
                request.get(MessageField.LOT_ID) instanceof Integer) {
            if (this.dbManager.checkAuth((String) request.get(MessageField.NAME),
                    (String) request.get(MessageField.PASSWORD))) {
                try {
                    this.auction.retractLot((String) request.get(MessageField.NAME), (Integer) request.get(MessageField.LOT_ID));
                    reply.put(MessageField.STATUS, StatusCode.OK);
                } catch (AuctionException e) {
                    this.packException(reply, e);
                }
            } else {
                reply.put(MessageField.STATUS, StatusCode.SERVER_ERROR);
                reply.put(MessageField.ANSWER, AnswerCode.WRONG_PASSWORD);
            }
        } else {
            reply.put(MessageField.STATUS, StatusCode.BAD_REQUEST);
        }
    }

    /**
     * Places new bid for a given lot.
     * Bid must follow lot's restrictions: >= top bid + minimal increment, lot has to be not finished, etc.
     * User must be logged in.
     *
     * @param request map, fields: name, password, lot_id, bid
     * @param reply   map
     */
    private void placeBid(Map<MessageField, Object> request, Map<MessageField, Object> reply) {
        if (this.isAuthValid(request) &&
                request.containsKey(MessageField.LOT_ID) &&
                request.get(MessageField.LOT_ID) instanceof Integer &&
                request.containsKey(MessageField.BID) &&
                request.get(MessageField.BID) instanceof Double) {
            if (this.dbManager.checkAuth((String) request.get(MessageField.NAME),
                    (String) request.get(MessageField.PASSWORD))) {
                try {
                    reply.put(MessageField.STATUS, StatusCode.OK);
                    reply.put(MessageField.LOT, this.auction.placeBid((String) request.get(MessageField.NAME),
                            (Integer) request.get(MessageField.LOT_ID),
                            (Double) request.get(MessageField.BID)));
                } catch (AuctionException e) {
                    this.packException(reply, e);
                }
            } else {
                reply.put(MessageField.STATUS, StatusCode.SERVER_ERROR);
                reply.put(MessageField.ANSWER, AnswerCode.WRONG_PASSWORD);
            }
        } else {
            reply.put(MessageField.STATUS, StatusCode.BAD_REQUEST);
        }
    }

    /**
     * Deletes given bid.
     * Lot must be not protected or finished, user must me logged in, only own bids can be retracted.
     *
     * @param request map, fields: name, password, lot_id
     * @param reply   map
     */
    private void retractBid(Map<MessageField, Object> request, Map<MessageField, Object> reply) {
        if (this.isAuthValid(request) &&
                request.containsKey(MessageField.LOT_ID) &&
                request.get(MessageField.LOT_ID) instanceof Integer) {
            if (this.dbManager.checkAuth((String) request.get(MessageField.NAME),
                    (String) request.get(MessageField.PASSWORD))) {
                try {
                    this.auction.retractBid((String) request.get(MessageField.NAME), (Integer) request.get(MessageField.LOT_ID));
                    reply.put(MessageField.STATUS, StatusCode.OK);
                } catch (AuctionException e) {
                    this.packException(reply, e);
                }
            } else {
                reply.put(MessageField.STATUS, StatusCode.SERVER_ERROR);
                reply.put(MessageField.ANSWER, AnswerCode.WRONG_PASSWORD);
            }
        } else {
            reply.put(MessageField.STATUS, StatusCode.BAD_REQUEST);
        }
    }
}
