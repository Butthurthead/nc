package com.ncedu.mindvr.auction.server.controller;


import com.ncedu.mindvr.auction.common.protocol.MessageField;
import com.ncedu.mindvr.auction.common.utils.Settings;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;

/**
 * Handles net interaction with multiple clients.
 * <p>
 * Server is not intended to be on high load, so in sake of simplicity following scenario is used:
 * connect->new thread->get request->send reply->disconnect.
 */
public class ObjectServer {
    private MessageManager messageManager;
    private Settings settings;


    public ObjectServer(MessageManager messageManager, Settings settings) {
        this.messageManager = messageManager;
        this.settings = settings;
    }

    /**
     * Listens to port from settings till interrupted.
     */
    public void listen() {
        try {
            int port = Integer.valueOf(settings.getValue("server-port"));
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Listening...");
            Socket clientSocket;
            while (!Thread.interrupted()) {
                clientSocket = serverSocket.accept();
                System.out.println("Client connected: " + clientSocket.toString());
                new Thread(new Worker(this.messageManager, clientSocket)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Nested class for handling requests.
     */
    private class Worker implements Runnable {
        private MessageManager messageManager;
        private Socket clientSocket;

        public Worker(MessageManager messageManager, Socket clientSocket) {
            this.messageManager = messageManager;
            this.clientSocket = clientSocket;
        }

        /**
         * Reads map from input stream, passes it to message manager and writes the reply.
         */
        @Override
        public void run() {
            try {
                ObjectOutput oOut = new ObjectOutputStream(this.clientSocket.getOutputStream());
                ObjectInput oIn = new ObjectInputStream(this.clientSocket.getInputStream());
                Object requestObject = oIn.readObject();
                if (requestObject instanceof Map) {
                    @SuppressWarnings("unchecked")
                    Map<MessageField, Object> request = (Map<MessageField, Object>) requestObject;
                    Map<MessageField, Object> reply = messageManager.processRequest(request);
                    oOut.writeObject(reply);
                }
                oOut.flush();
                this.clientSocket.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
