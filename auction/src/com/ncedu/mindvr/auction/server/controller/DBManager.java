package com.ncedu.mindvr.auction.server.controller;

import com.ncedu.mindvr.auction.common.model.ServerLot;
import com.ncedu.mindvr.auction.common.utils.Settings;

import java.io.File;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.sql.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * Database controller, handles users' authorization and keeps auction's state consistent.
 */
public class DBManager {

    /**
     * DB Connection.
     */
    private Connection connection;

    /**
     * Default constructor.
     * Connects to SQLite database specified in dp-path field of settings.
     * If the specified file doesn't exist it will be created and tables needed will be created.
     *
     * @param settings Settings object.
     */
    public DBManager(Settings settings) {
        String dbPath = settings.getValue("db-path");
        if (!new File(dbPath).exists()) {
            try {
                this.connection = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                this.connection.setAutoCommit(false);
                this.initDB();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            try {
                this.connection = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                this.connection.setAutoCommit(false);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Creates users, lots and bids tables in fresh database.
     */
    public void initDB() {
        try {
            Statement statement = this.connection.createStatement();
            statement.execute(DBManagerStatic.createUsersTable);
            statement.execute(DBManagerStatic.createLotsTable);
            statement.execute(DBManagerStatic.createBidsTable);
            this.connection.commit();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads and constructs integer:lot map from database.
     * Method for auction constructor.
     *
     * @return Integer:ServerLot map from database.
     */
    public Map<Integer, ServerLot> loadLots() {
        Map<Integer, ServerLot> lots = new HashMap<>();
        try {
            PreparedStatement selectBidsStatement = this.connection.prepareStatement(
                    "SELECT bidder, bid FROM bids WHERE lot_id=?");
            Statement statement = this.connection.createStatement();
            ResultSet selectAllResult = statement.executeQuery("SELECT * FROM lots");
            while (selectAllResult.next()) {
                int id = selectAllResult.getInt("lot_id");
                String sellerName = selectAllResult.getString("seller");
                String title = selectAllResult.getString("title");
                String description = selectAllResult.getString("description");
                double minPrice = selectAllResult.getDouble("min_price");
                double maxPrice = selectAllResult.getDouble("max_price");
                double minIncrement = selectAllResult.getDouble("min_increment");
                ZonedDateTime auctionEnd = ZonedDateTime.ofInstant(Instant.ofEpochSecond(selectAllResult.getLong("auction_end")),
                        ZoneId.systemDefault());
                int snipeDuration = selectAllResult.getInt("snipe_duration");
                int snipeIncrement = selectAllResult.getInt("snipe_increment");
                boolean finished = false;
                if (selectAllResult.getInt("finished") == 1) {
                    finished = true;
                }
                double topBid = selectAllResult.getDouble("top_bid");
                String topBidder = selectAllResult.getString("top_bidder");

                Map<String, Double> bids = new HashMap<>();
                selectBidsStatement.setInt(1, id);
                ResultSet rsBids = selectBidsStatement.executeQuery();
                while (rsBids.next()) {
                    bids.put(rsBids.getString("bidder"), rsBids.getDouble("bid"));
                }
                lots.put(id,
                        new ServerLot(id, sellerName, title, description,
                                minPrice, maxPrice, minIncrement, auctionEnd,
                                snipeDuration, snipeIncrement, finished, bids,
                                topBid, topBidder));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lots;
    }

    /**
     * Updates lot in DB.
     * Generally is called on every lot change to keep auction's states in memory and DB synchronized.
     *
     * @param lot ServerLot to be updated.
     */
    public void updateLot(ServerLot lot) {
        try {
            PreparedStatement ps = this.connection.prepareStatement(
                    "DELETE FROM bids WHERE lot_id=?");
            ps.setInt(1, lot.getId());
            ps.executeUpdate();
            ps.close();
            ps = this.connection.prepareStatement(
                    "DELETE FROM lots WHERE lot_id=?");
            ps.setInt(1, lot.getId());
            ps.executeUpdate();
            ps.close();
            ps = this.connection.prepareStatement(DBManagerStatic.newLot);
            ps.setInt(1, lot.getId());
            ps.setString(2, lot.getSellerName());
            ps.setString(3, lot.getTitle());
            ps.setString(4, lot.getDescription());
            ps.setDouble(5, lot.getMinPrice());
            ps.setDouble(6, lot.getMaxPrice());
            ps.setDouble(7, lot.getMinIncrement());
            ps.setLong(8, lot.getAuctionEnd().toEpochSecond());
            ps.setInt(9, lot.getSnipeDuration());
            ps.setInt(10, lot.getSnipeIncrement());
            if (lot.isFinished()) {
                ps.setInt(11, 1);
            } else {
                ps.setInt(11, 0);
            }
            ps.setDouble(12, lot.getTopBid());
            ps.setString(13, lot.getTopBidder());
            ps.executeUpdate();
            ps.close();
            ps = this.connection.prepareStatement(
                    "INSERT INTO bids (lot_id, bidder, bid) VALUES (?,?,?)");
            if (!lot.getBids().isEmpty()) {
                for (Map.Entry<String, Double> entry : lot.getBids().entrySet()) {
                    ps.setInt(1, lot.getId());
                    ps.setString(2, entry.getKey());
                    ps.setDouble(3, entry.getValue());
                    ps.executeUpdate();
                }
            }
            this.connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes lot from the database.
     *
     * @param lot ServerLot to be deleted.
     */
    public void deleteLot(ServerLot lot) {
        try {
            PreparedStatement ps = this.connection.prepareStatement(
                    "DELETE FROM bids WHERE lot_id=?");
            ps.setInt(1, lot.getId());
            ps.executeUpdate();
            ps.close();
            ps = this.connection.prepareStatement(
                    "DELETE FROM lots WHERE lot_id=?");
            ps.setInt(1, lot.getId());
            ps.executeUpdate();
            ps.close();
            this.connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns maximum lot id present in database
     *
     * @return integer, top lot id.
     */
    public int getTopLotId() {
        try {
            Statement statement = this.connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT max(lot_id) FROM lots");
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Checks if specified user is registered (present in DB).
     *
     * @param name username (String) to be checked.
     * @return true if user exists, false otherwise.
     */
    public boolean userExist(String name) {
        try {
            PreparedStatement statement = this.connection.prepareStatement(
                    "SELECT login FROM users WHERE login = ?");
            statement.setString(1, name);
            return statement.executeQuery().next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Checks if provided name:password pair is correct.
     *
     * @param name     username (String)
     * @param password password (String)
     * @return true if name:password is correct.
     */
    public boolean checkAuth(String name, String password) {
        boolean authenticated = false;
        try {
            PreparedStatement ps = this.connection.prepareStatement(
                    "SELECT salt, password FROM users WHERE login=?");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String digest = rs.getString("password");
                String salt = rs.getString("salt");
                byte[] bDigest = Base64.getDecoder().decode(digest);
                byte[] bSalt = Base64.getDecoder().decode(salt);
                byte[] newDigest = getHash(password, bSalt);
                authenticated = Arrays.equals(newDigest, bDigest);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return authenticated;
    }

    /**
     * Creates new salted username record.
     *
     * @param name     username (String)
     * @param password password (String)
     */
    public void registerUser(String name, String password) {
        try {
            SecureRandom random = new SecureRandom();
            byte[] bSalt = new byte[8];
            random.nextBytes(bSalt);
            byte[] bDigest = getHash(password, bSalt);
            String sDigest = Base64.getEncoder().encodeToString(bDigest);
            String sSalt = Base64.getEncoder().encodeToString(bSalt);
            PreparedStatement statement = this.connection.prepareStatement(
                    "INSERT INTO users (login, salt, password) VALUES (?,?,?)");
            statement.setString(1, name);
            statement.setString(2, sSalt);
            statement.setString(3, sDigest);
            statement.executeUpdate();
            this.connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates existing user's password.
     *
     * @param name     username (String)
     * @param password password (String)
     */
    public void updatePassword(String name, String password) {
        try {
            SecureRandom random = new SecureRandom();
            byte[] bSalt = new byte[8];
            random.nextBytes(bSalt);
            byte[] bDigest = getHash(password, bSalt);
            String sDigest = Base64.getEncoder().encodeToString(bDigest);
            String sSalt = Base64.getEncoder().encodeToString(bSalt);
            PreparedStatement statement = this.connection.prepareStatement(
                    "UPDATE users SET salt=?, password=? WHERE login=?");
            statement.setString(3, name);
            statement.setString(1, sSalt);
            statement.setString(2, sDigest);
            statement.executeUpdate();
            this.connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns hash for given salt and password strings.
     *
     * @param password password (String)
     * @param salt     salt(byte[])
     * @return byte[] hash
     */
    private byte[] getHash(String password, byte[] salt) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            digest.update(salt);
            return digest.digest(password.getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
