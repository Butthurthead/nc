package com.ncedu.mindvr.auction.server.controller;

/**
 * Contains long SQL Statements for DBManager class.
 * @see DBManager
 */
public class DBManagerStatic {
    public static final String createUsersTable = "CREATE TABLE `users` (\n" +
            "\t`login`\tTEXT NOT NULL UNIQUE,\n" +
            "\t`salt`\tTEXT NOT NULL,\n" +
            "\t`password`\tTEXT NOT NULL,\n" +
            "\tPRIMARY KEY(login)\n" +
            ");";
    public static final String createLotsTable = "CREATE TABLE `lots` (\n" +
            "\t`lot_id`\tINTEGER NOT NULL UNIQUE,\n" +
            "\t`seller`\tTEXT NOT NULL,\n" +
            "\t`title`\tTEXT NOT NULL,\n" +
            "\t`description`\tTEXT,\n" +
            "\t`min_price`\tREAL NOT NULL,\n" +
            "\t`max_price`\tREAL NOT NULL,\n" +
            "\t`min_increment`\tREAL NOT NULL,\n" +
            "\t`auction_end`\tINTEGER NOT NULL,\n" +
            "\t`snipe_duration`\tINTEGER NOT NULL,\n" +
            "\t`snipe_increment`\tINTEGER NOT NULL,\n" +
            "\t`finished`\tINTEGER NOT NULL,\n" +
            "\t`top_bid`\tREAL,\n" +
            "\t`top_bidder`\tTEXT,\n" +
            "\tPRIMARY KEY(lot_id)\n" +
            ");";
    public static final String createBidsTable = "CREATE TABLE `bids` (\n" +
            "\t`lot_id`\tINTEGER NOT NULL,\n" +
            "\t`bidder`\tTEXT NOT NULL,\n" +
            "\t`bid`\tREAL NOT NULL\n" +
            ");";
    public static final String newLot = "INSERT INTO lots (lot_id, seller, title, description, " +
            "min_price, max_price, min_increment, " +
            "auction_end, snipe_duration, snipe_increment, " +
            "finished, top_bid, top_bidder) " +
            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
}
