package com.ncedu.mindvr.auction.server.model;

import com.ncedu.mindvr.auction.common.model.AuctionException;
import com.ncedu.mindvr.auction.common.model.CompactLot;
import com.ncedu.mindvr.auction.common.model.ServerLot;
import com.ncedu.mindvr.auction.server.controller.DBManager;

import java.util.Map;

/**
 * Represents auction as map of lots and corresponding methods.
 * Methods with auth required assume that password is already checked.
 */
public class Auction {


    private java.util.Map<Integer, ServerLot> lots;
    private DBManager dbManager;
    private int topLotId;

    public Auction(DBManager dbManager) {
        this.dbManager = dbManager;
        this.lots = this.dbManager.loadLots();
        this.topLotId = this.dbManager.getTopLotId();
    }

    public Map<Integer, ServerLot> getLots() {
        //Refreshes finished state
        this.lots.values().forEach(ServerLot::isFinished);
        return lots;
    }

    public ServerLot getLot(int lotId) throws AuctionException {
        if (this.lots.containsKey(lotId)) {
            return this.lots.get(lotId);
        } else {
            throw new AuctionException("No such lot");
        }
    }

    public synchronized ServerLot placeLot(String sellerName, CompactLot lot) {
        this.topLotId++;
        ServerLot newServerLot = new ServerLot(this.topLotId, sellerName, lot);
        this.lots.put(this.topLotId, newServerLot);
        this.dbManager.updateLot(newServerLot);
        return newServerLot;
    }

    public synchronized void retractLot(String sellerName, int lotId) throws AuctionException {
        if (this.lots.containsKey(lotId)) {
            ServerLot lot = this.lots.get(lotId);
            if (lot.getSellerName().equals(sellerName)) {
                this.dbManager.deleteLot(lot);
                this.lots.remove(lotId);
            } else {
                throw new AuctionException("Retracting not owned lot");
            }
        } else {
            throw new AuctionException("No such lot");
        }
    }

    public ServerLot placeBid(String bidderName, int lotId, double bid) throws AuctionException {
        if (this.lots.containsKey(lotId)) {
            ServerLot lot = this.lots.get(lotId);
            lot.placeBid(bidderName, bid);
            this.dbManager.updateLot(lot);
            return lot;
        } else {
            throw new AuctionException("No such lot");
        }
    }

    public void retractBid(String bidderName, int lotId) throws AuctionException {
        if (this.lots.containsKey(lotId)) {
            ServerLot lot = this.lots.get(lotId);
            lot.retractBid(bidderName);
            this.dbManager.updateLot(lot);
        } else {
            throw new AuctionException("No such lot");
        }
    }

}
