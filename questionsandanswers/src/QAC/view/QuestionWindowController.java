package QAC.view;

import QAC.Main;
import QAC.model.Answer;
import QAC.model.Question;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;


public class QuestionWindowController {

    private Main main;

    @FXML
    private Label textTheme;
    @FXML
    private Label textQuestion;
    @FXML
    private TableView<Answer> tableAnswers;
    @FXML
    private TableColumn<Answer, String> answerAuthor;
    @FXML
    private TableColumn<Answer, String> answerValue;
    @FXML
    private TableColumn<Answer, Number> answerLikes;

    private Stage dialogStage;
    private Question question;

    //@ ����� �����------>
    @FXML
    private TextArea newAnswerValue;

    @FXML
    private void sendIt() {
        if (!newAnswerValue.getText().equals("")) {
            Answer answer = new Answer(main.getCurrentUser().getId(), question.getId(), newAnswerValue.getText());
            Main.bDworker.insert_A(answer);
            main.setAnswersList(Main.bDworker.selectAllAnswers());
            newAnswerValue.setText("");
            renewTableAnswers();
        }
    }


    ///@ ������ ��������
    @FXML
    private void ratingUp() {
        Main.bDworker.updateRatingQuestion(question, true);

    }

    @FXML
    private void ratingDown() {
        Main.bDworker.updateRatingQuestion(question, false);

    }


    @FXML
    private void initialize() {

        //������������ ������ ������ ���� ����� � �������---->
        answerValue.setCellFactory(new Callback<TableColumn<Answer, String>, TableCell<Answer, String>>() {
            @Override
            public TableCell<Answer, String> call(TableColumn<Answer, String> param) {
                final TableCell<Answer, String> cell = new TableCell<Answer, String>() {
                    private Text text;

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            text = new Text(item);
                            text.wrappingWidthProperty().bind(getTableColumn().widthProperty());
                            text.fontProperty().bind(fontProperty());
                            setGraphic(text);
                        }
                    }
                };
                return cell;
            }
        });
        //<--------------������������ ������ ���� ����� � �������

        //��������� ������� �������
        answerAuthor.setCellValueFactory(cellData -> cellData.getValue().getAnswerAuthor().loginProperty());
        answerValue.setCellValueFactory(cellData -> cellData.getValue().answValueProperty());
        answerLikes.setCellValueFactory(cellData -> cellData.getValue().answLikesProperty());
    }

    public void setDialogStage(Stage dialogStage, Main main, Question question) {
        this.dialogStage = dialogStage;
        this.main = main;
        this.question = question;
        renewTableAnswers();
        textTheme.setText(question.getTheme());
        textQuestion.setText(question.getQuestion());
    }

    private void renewTableAnswers() {
        ObservableList<Answer> newList = FXCollections.observableArrayList();
        for (int i = 0; i < main.getAnswersList().size(); i++) {
            if (main.getAnswersList().get(i).getQuestionID() == question.getId())
                newList.add(main.getAnswersList().get(i));
        }
        tableAnswers.setItems(newList);
    }
}
