package QAC.view;

import QAC.Main;
import QAC.model.Question;
import QAC.model.User;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class MainWindowController {
    @FXML
    private TableView<Question> tableView;
    @FXML
    private TableColumn<Question, String> questionTheme;
    @FXML
    private TableColumn<Question, Number> questionNumberAnsw;
    @FXML
    private TableColumn<Question, Number> questionRating;
    @FXML
    private TableColumn<Question, String> questionAuthor;

    @FXML
    private Label userLogin;
    @FXML
    private Label userFullName;
    @FXML
    private Label userNumQ;
    @FXML
    private Label userNumA;
    @FXML
    private Label userCarm;

    private User user;

    //@ ����� ������----->
    @FXML
    private TextField newQuestionTheme;
    @FXML
    private TextArea newQuestionValue;
    private Main main;

    public MainWindowController() {
    }

    @FXML
    private void sendIt() {
        if (isQuestionOk()) {
            Main.bDworker.insert_Q(new Question(user.getId(), newQuestionTheme.getText(), newQuestionValue.getText()));
            renew();
            newQuestionTheme.setText("");
            newQuestionValue.setText("");
        }
    }
    //--

    private boolean isQuestionOk() {
        return !(newQuestionTheme.getText().equals("") || newQuestionValue.getText().equals(""));
    }

    //@ ������ ����������----->
    @FXML
    private void renew() {
        tableView.setItems(Main.bDworker.selectAllQuestions());
        main.setQuestionsList(Main.bDworker.selectAllQuestions());
        main.setUsersList(Main.bDworker.selectAllUsers());
        main.setAnswersList(Main.bDworker.selectAllAnswers());
        userNumQ.setText(Integer.toString(Main.bDworker.getUser(main.getCurrentUser().getId()).getNumberOfQuestions()));
        userNumA.setText(Integer.toString(Main.bDworker.getUser(main.getCurrentUser().getId()).getNumberOfAnswers()));
        userCarm.setText(Integer.toString(Main.bDworker.getUser(main.getCurrentUser().getId()).getCarm()));
    }

    @FXML
    private void initialize() {
        questionTheme.setCellValueFactory(cellData -> cellData.getValue().themeProperty());
        questionNumberAnsw.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue().getCountAnswers()));
        questionRating.setCellValueFactory(cellData -> cellData.getValue().ratingProperty());
        questionAuthor.setCellValueFactory(cellData -> cellData.getValue().getAuthor().loginProperty());

        //����������� ���� ���� �� �������
        tableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() > 1) {
                    showQuestionDetails(tableView.getSelectionModel().getSelectedItem());
                }
            }
        });
    }

    private void showQuestionDetails(Question newValue) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/QuestionWindow.fxml"));
            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Quetion: " + newValue.getTheme());
            dialogStage.initModality(Modality.WINDOW_MODAL);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            QuestionWindowController controller = loader.getController();
            controller.setDialogStage(dialogStage, main, newValue);

            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setMain(Main main) {
        this.main = main;
        tableView.setItems(Main.bDworker.selectAllQuestions());
    }

    public void setUserData(User user) {
        if (user != null) {
            this.user = user;
            userLogin.setText(user.getLogin());
            userFullName.setText(user.getFirstName() + " " + user.getLastName());
            userNumQ.setText(Integer.toString(user.getNumberOfQuestions()));
            userNumA.setText(Integer.toString(user.getNumberOfAnswers()));
            userCarm.setText(Integer.toString(user.getCarm()));
        }
    }
}
