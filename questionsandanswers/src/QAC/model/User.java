package QAC.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class User {
    private final StringProperty firstName;
    private final StringProperty lastName;
    private final StringProperty login;
    //    private final StringProperty pass;
    private final IntegerProperty numberOfQuestions;
    private final IntegerProperty numberOfAnswers;
    private final IntegerProperty id;
    private final IntegerProperty carm;

    public User(String name, String lastName) {
        this.firstName = new SimpleStringProperty(name);
        this.lastName = new SimpleStringProperty(lastName);

        this.login = new SimpleStringProperty("login");
//        this.pass = new SimpleStringProperty("pass");
        this.numberOfQuestions = new SimpleIntegerProperty(0);
        this.numberOfAnswers = new SimpleIntegerProperty(0);
        this.carm = new SimpleIntegerProperty(0);
        this.id = new SimpleIntegerProperty(1);
    }

    public User(String firstName, String lastName, String login,
                Integer numberOfQuestions, Integer numberOfAnswers, Integer id, Integer carm) {
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.login = new SimpleStringProperty(login);
//        this.pass = pass;
        this.numberOfQuestions = new SimpleIntegerProperty(numberOfQuestions);
        this.numberOfAnswers = new SimpleIntegerProperty(numberOfAnswers);
        this.id = new SimpleIntegerProperty(id);
        this.carm = new SimpleIntegerProperty(carm);
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

//    public String getPass() {
//        return pass.get();
//    }
//
//    public StringProperty passProperty() {
//        return pass;
//    }
//
//    public void setPass(String pass) {
//        this.pass.set(pass);
//    }

    public String getLogin() {
        return login.get();
    }

    public void setLogin(String login) {
        this.login.set(login);
    }

    public StringProperty loginProperty() {
        return login;
    }

    public int getNumberOfQuestions() {
        return numberOfQuestions.get();
    }

    public void setNumberOfQuestions(int numberOfQuestions) {
        this.numberOfQuestions.set(numberOfQuestions);
    }

    public IntegerProperty numberOfQuestionsProperty() {
        return numberOfQuestions;
    }

    public int getNumberOfAnswers() {
        return numberOfAnswers.get();
    }

    public void setNumberOfAnswers(int numberOfAnswers) {
        this.numberOfAnswers.set(numberOfAnswers);
    }

    public IntegerProperty numberOfAnswersProperty() {
        return numberOfAnswers;
    }

    public int getCarm() {
        return carm.get();
    }

    public void setCarm(int carm) {
        this.carm.set(carm);
    }

    public IntegerProperty carmProperty() {
        return carm;
    }
}
