package QAC.model;

import QAC.Main;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Question {
    private final StringProperty theme;
    private final StringProperty question;
    private final IntegerProperty authorID;
    private final IntegerProperty id;
    private final IntegerProperty countAnswers;
    private final IntegerProperty rating;

    public Question(Integer authorID, String theme, String question) {
        this.authorID = new SimpleIntegerProperty(authorID);
        this.question = new SimpleStringProperty(question);
        this.theme = new SimpleStringProperty(theme);
        this.rating = new SimpleIntegerProperty(0);
        this.id = new SimpleIntegerProperty(1);
        this.countAnswers = new SimpleIntegerProperty(0);
    }

    public Question(Integer id, Integer rating, Integer authorID, String question, String theme, int countAnswers) {
        this.id = new SimpleIntegerProperty(id);
        this.rating = new SimpleIntegerProperty(rating);
        this.question = new SimpleStringProperty(question);
        this.theme = new SimpleStringProperty(theme);
        this.countAnswers = new SimpleIntegerProperty(countAnswers);
        this.authorID = new SimpleIntegerProperty(authorID);
    }

    public int getCountAnswers() {
        return countAnswers.get();
    }

    public void setCountAnswers(int countAnswers) {
        this.countAnswers.set(countAnswers);
    }

    public IntegerProperty countAnswersProperty() {
        return countAnswers;
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public int getAuthorID() {
        return authorID.get();
    }

    public void setAuthorID(int authorID) {
        this.authorID.set(authorID);
    }

    public User getAuthor() {
        return Main.bDworker.getQusetionAuthor(this);
    }

    public IntegerProperty authorIDProperty() {
        return authorID;
    }

    public int getRating() {
        return rating.get();
    }

    public void setRating(int rating) {
        this.rating.set(rating);
    }

    public IntegerProperty ratingProperty() {
        return rating;
    }

    public String getTheme() {
        return theme.get();
    }

    public void setTheme(String theme) {
        this.theme.set(theme);
    }

    public StringProperty themeProperty() {
        return theme;
    }

    public String getQuestion() {
        return question.get();
    }

    public void setQuestion(String question) {
        this.question.set(question);
    }

    public StringProperty questionProperty() {
        return question;
    }
}
