package QAC.model;


import QAC.Main;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Answer {
    private final IntegerProperty questionID;
    private final StringProperty answValue;
    private final IntegerProperty answAuthor;
    private final IntegerProperty answLikes;
    private final IntegerProperty id;

    public Answer(Integer answAuthor, Integer questionID, String value) {
        this.answAuthor = new SimpleIntegerProperty(answAuthor);
        this.questionID = new SimpleIntegerProperty(questionID);
        this.answValue = new SimpleStringProperty(value);
        this.answLikes = new SimpleIntegerProperty(0);
        this.id = new SimpleIntegerProperty(1);
    }

    public Answer(Integer id, Integer questionID, String answValue, Integer answAuthor, Integer answLikes) {
        this.questionID = new SimpleIntegerProperty(questionID);
        this.answValue = new SimpleStringProperty(answValue);
        this.answAuthor = new SimpleIntegerProperty(answAuthor);
        this.answLikes = new SimpleIntegerProperty(answLikes);
        this.id = new SimpleIntegerProperty(id);
    }

    public int getQuestionID() {
        return questionID.get();
    }

    public void setQuestionID(int questionID) {
        this.questionID.set(questionID);
    }

    public IntegerProperty questionIDProperty() {
        return questionID;
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public String getAnswValue() {
        return answValue.get();
    }

    public void setAnswValue(String answValue) {
        this.answValue.set(answValue);
    }

    public StringProperty answValueProperty() {
        return answValue;
    }

    public int getAnswAuthor() {
        return answAuthor.get();
    }

    public void setAnswAuthor(int answAuthor) {
        this.answAuthor.set(answAuthor);
    }

    public IntegerProperty answAuthorProperty() {
        return answAuthor;
    }

    public User getAnswerAuthor() {
        return Main.bDworker.getAnswerAuthor(this);
    }

    public int getAnswLikes() {
        return answLikes.get();
    }

    public void setAnswLikes(int answLikes) {
        this.answLikes.set(answLikes);
    }

    public IntegerProperty answLikesProperty() {
        return answLikes;
    }
}
