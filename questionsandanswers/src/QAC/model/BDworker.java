package QAC.model;

import com.mysql.jdbc.Driver;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BDworker {

    private static final String URL = "jdbc:mysql://localhost:3306/bd1?user=root&password=root";
    private static final String INSERT_NEW_QUESTION = "insert into questions(Title, Question, Rating, Author) values(?,?,?,?)";
    private static final String INSERT_NEW_ANSWER = "insert into answers(id_questions, Answer, Author, countLikes) values(?,?,?,?)";
    private static final String INSERT_NEW_USERS = "insert into users(login, fullname, Carma)  values(?,?,?,?,?,?)";
    private static final String GET_ALL_Q = "SELECT * FROM questions";
    private static final String GET_ALL_A = "SELECT * FROM answers";
    private static final String GET_ALL_U = "SELECT * FROM users";
    private static final String DELETE_Q = "Delete from questions where id = ?";
    private static final String DELETE_A = "Delete from answers where id = ?";
    private static final String DELETE_U = "Delete from users where id = ?";
    private static final String UPDATE_RATING_QUESTION = "UPDATE QUESTIONs set rating = ? where idQuestion = ?";
    PreparedStatement preparedStatement = null;
    Connection connection = null;

    public void bdworker() {

        try {
            File jdbcLib = new File("mysql-connector-java-5.1.37.jar");
            System.out.println(jdbcLib.getAbsolutePath());
            URLClassLoader loader = new URLClassLoader(new URL[] {new URL("jar:file:///"+jdbcLib.getAbsolutePath()+"!/")});
            Driver driver = (Driver) loader.loadClass("com.mysql.jdbc.Driver").newInstance();
            connection = driver.connect(URL,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_Q(Question question) {
        try {

            preparedStatement = connection.prepareStatement(INSERT_NEW_QUESTION);

            preparedStatement.setString(1, question.getTheme());
            preparedStatement.setString(2, question.getQuestion());
            preparedStatement.setInt(3, question.getRating());
            preparedStatement.setInt(4, question.getAuthorID());

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void insert_A(Answer answer) {
        try {

            preparedStatement = connection.prepareStatement(INSERT_NEW_ANSWER);

            preparedStatement.setInt(1, answer.getQuestionID());
            preparedStatement.setString(2, answer.getAnswValue());
            preparedStatement.setInt(3, answer.getAnswAuthor());
            preparedStatement.setInt(4, answer.getAnswLikes());

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void insert_U(User user) {
        try {

            preparedStatement = connection.prepareStatement(INSERT_NEW_USERS);

            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getFirstName() + " " + user.getLastName());
            preparedStatement.setInt(3, user.getNumberOfQuestions());
            preparedStatement.setInt(4, user.getNumberOfAnswers());
            preparedStatement.setInt(5, user.getCarm());

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    //Remove question
    public void remove_Q(Integer id) {
        try {
            preparedStatement = connection.prepareStatement(DELETE_Q);
            preparedStatement.setInt(1, id);
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //������ �������������
    public ObservableList<User> selectAllUsers() {
        ObservableList<User> selected = FXCollections.observableArrayList();
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_U);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String firstName = "";
                String lastName = "";
                if (resultSet.getString("fullName").split(" ").length == 2) {
                    firstName = resultSet.getString("fullName").split(" ")[0];
                    lastName = resultSet.getString("fullName").split(" ")[1];
                }
                String login = resultSet.getString("login");
                int id = resultSet.getInt("idUsers");
                int countOfQuestions = 0;
                preparedStatement = connection.prepareStatement("SELECT COUNT(*) AS 'COQ' FROM questions WHERE Author = ?");
                preparedStatement.setInt(1, id);
                ResultSet count = preparedStatement.executeQuery();
                while (count.next()) {
                    countOfQuestions = count.getInt(1);
                }
                int countOfAnswers = 0;
                preparedStatement = connection.prepareStatement("SELECT COUNT(*) AS 'COA' FROM answers WHERE Author = ?");
                preparedStatement.setInt(1, id);
                count = preparedStatement.executeQuery();
                while (count.next()) {
                    countOfAnswers = count.getInt(1);
                }
                int carma = resultSet.getInt("Carma");

                selected.add(new User(firstName, lastName, login, countOfQuestions, countOfAnswers, id, carma));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return selected;
    }

    //������ ��������
    public ObservableList<Question> selectAllQuestions() {
        ObservableList<Question> selected = FXCollections.observableArrayList();
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_Q);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int idQ = resultSet.getInt("idQuestion");
                int rating = resultSet.getInt("Rating");
                int author = resultSet.getInt("Author");
                int answers = 0;
                preparedStatement = connection.prepareStatement("SELECT count(*) AS 'RES' from answers WHERE id_questions = ?");
                preparedStatement.setInt(1, idQ);
                ResultSet count = preparedStatement.executeQuery();
                while (count.next()) {
                    answers = count.getInt("RES");
                }
                String title = resultSet.getString("Title");
                String Question = resultSet.getString("Question");
                selected.add(new Question(idQ, rating, author, Question, title, answers));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return selected;
    }

    //������ �������
    public ObservableList<Answer> selectAllAnswers() {
        ObservableList<Answer> selected = FXCollections.observableArrayList();
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_A);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int idQuestion = resultSet.getInt("id_questions");
                String answer = resultSet.getString("Answer");
                int author = resultSet.getInt("Author");
                int countLikes = resultSet.getInt("countLikes");
                selected.add(new Answer(id, idQuestion, answer, author, countLikes));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return selected;
    }

    //����� �������.
    public User getQusetionAuthor(Question question) {
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE idUsers = ?");
            preparedStatement.setInt(1, question.getAuthorID());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String firstName = "";
                String lastName = "";
                if (resultSet.getString("fullName").split(" ").length == 2) {
                    firstName = resultSet.getString("fullName").split(" ")[0];
                    lastName = resultSet.getString("fullName").split(" ")[1];
                }
                String login = resultSet.getString("login");
                int carma = resultSet.getInt("Carma");
                int id = resultSet.getInt("idUsers");
                int countOfQuestions = 0;
                preparedStatement = connection.prepareStatement("SELECT COUNT(*) AS 'COQ' FROM questions WHERE Author = ?");
                preparedStatement.setInt(1, id);
                ResultSet count = preparedStatement.executeQuery();
                while (count.next()) {
                    countOfQuestions = count.getInt(1);
                }
                int countOfAnswers = 0;
                preparedStatement = connection.prepareStatement("SELECT COUNT(*) AS 'COA' FROM answers WHERE Author = ?");
                preparedStatement.setInt(1, id);
                count = preparedStatement.executeQuery();
                while (count.next()) {
                    countOfQuestions = count.getInt(1);
                }

                return new User(firstName, lastName, login, countOfQuestions, countOfAnswers, id, carma);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //����� ������.
    public User getAnswerAuthor(Answer answer) {
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE idUsers = ?");
            preparedStatement.setInt(1, answer.getAnswAuthor());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String firstName = "";
                String lastName = "";
                if (resultSet.getString("fullName").split(" ").length == 2) {
                    firstName = resultSet.getString("fullName").split(" ")[0];
                    lastName = resultSet.getString("fullName").split(" ")[1];
                }
                String login = resultSet.getString("login");
                int carma = resultSet.getInt("Carma");
                int id = resultSet.getInt("idUsers");
                int countOfQuestions = 0;
                preparedStatement = connection.prepareStatement("SELECT COUNT(*) AS 'COQ' FROM questions WHERE Author = ?");
                preparedStatement.setInt(1, id);
                ResultSet count = preparedStatement.executeQuery();
                while (count.next()) {
                    countOfQuestions = count.getInt(1);
                }
                int countOfAnswers = 0;
                preparedStatement = connection.prepareStatement("SELECT COUNT(*) AS 'COA' FROM answers WHERE Author = ?");
                preparedStatement.setInt(1, id);
                count = preparedStatement.executeQuery();
                while (count.next()) {
                    countOfQuestions = count.getInt(1);
                }

                return new User(firstName, lastName, login, countOfQuestions, countOfAnswers, id, carma);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //user
    public User getUser(Integer idUser) {
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM users WHERE idUsers = ?");
            preparedStatement.setInt(1, idUser);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String firstName = "";
                String lastName = "";
                if (resultSet.getString("fullName").split(" ").length == 2) {
                    firstName = resultSet.getString("fullName").split(" ")[0];
                    lastName = resultSet.getString("fullName").split(" ")[1];
                }
                String login = resultSet.getString("login");
                int carma = resultSet.getInt("Carma");
                int id = resultSet.getInt("idUsers");
                int countOfQuestions = 0;
                preparedStatement = connection.prepareStatement("SELECT COUNT(*) AS 'COQ' FROM questions WHERE Author = ?");
                preparedStatement.setInt(1, id);
                ResultSet count = preparedStatement.executeQuery();
                while (count.next()) {
                    countOfQuestions = count.getInt(1);
                }
                int countOfAnswers = 0;
                preparedStatement = connection.prepareStatement("SELECT COUNT(*) AS 'COA' FROM answers WHERE Author = ?");
                preparedStatement.setInt(1, id);
                count = preparedStatement.executeQuery();
                while (count.next()) {
                    countOfAnswers = count.getInt(1);
                }
                return new User(firstName, lastName, login, countOfQuestions, countOfAnswers, id, carma);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateRatingQuestion(Question question, Boolean idButton) {
        try {

            preparedStatement = connection.prepareStatement(UPDATE_RATING_QUESTION);
            preparedStatement.setInt(1, idButton ? question.getRating() + 1 : question.getRating() - 1);
            preparedStatement.setInt(2, question.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
