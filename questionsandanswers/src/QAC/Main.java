package QAC;

import QAC.model.Answer;
import QAC.model.BDworker;
import QAC.model.Question;
import QAC.model.User;
import QAC.view.MainWindowController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    public void setAnswersList(ObservableList<Answer> answersList) {
        this.answersList = answersList;
    }

    public void setQuestionsList(ObservableList<Question> questionsList) {
        this.questionsList = questionsList;
    }

    public void setUsersList(ObservableList<User> usersList) {
        this.usersList = usersList;
    }

    //@ ������ ������, �������, ��������, �� �����, ����� ���������.
    private static ObservableList<Question> questionsList = FXCollections.observableArrayList();
    private ObservableList<Answer> answersList = FXCollections.observableArrayList();
    private ObservableList<User> usersList = FXCollections.observableArrayList();
    private User currentUser;
    private Stage primaryStage;

    public static ObservableList<Question> getQuestionsList(){
        return questionsList;
    }

    public ObservableList<Answer> getAnswersList(){
        return answersList;
    }

    public ObservableList<User> getUsersList() {
        return usersList;
    }

    //����������� � ����.
    public static BDworker bDworker = new BDworker();


    public User getCurrentUser(){
        return currentUser;
    }

    public Main(){
        bDworker.bdworker();
        usersList = bDworker.selectAllUsers();
        //��������
        currentUser = usersList.get(0);
        questionsList = bDworker.selectAllQuestions();
        answersList = bDworker.selectAllAnswers();
//        try {
//            Class.forName("java.sql.Driver");
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
    }


    public void showUserOverview() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/MainWindow.fxml"));
            Parent personOverview = loader.load();

            primaryStage.setScene(new Scene(personOverview));
            primaryStage.show();

            MainWindowController controller = loader.getController();
            controller.setMain(this);
            controller.setUserData(currentUser);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Q&A");
        showUserOverview();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
