-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: bd1
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_questions` int(11) DEFAULT NULL,
  `Answer` varchar(255) DEFAULT NULL,
  `Author` int(11) DEFAULT NULL,
  `countLikes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `aqID_FK_idx` (`id_questions`),
  KEY `aaID_FK_idx` (`Author`),
  CONSTRAINT `aaID_FK` FOREIGN KEY (`Author`) REFERENCES `users` (`idUsers`) ON UPDATE NO ACTION,
  CONSTRAINT `aqID_FK` FOREIGN KEY (`id_questions`) REFERENCES `questions` (`idQuestion`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,1,'asdsadsafassa',1,0),(2,2,'sadsadasdadsaasf',1,0),(3,2,'sadsffxzvcxbvdsds',1,0),(4,1,'fdfdsf',1,0),(5,1,'gdfgfdfgfd',1,0),(6,8,'qweqwe',1,0),(7,8,'wqeqwe',1,0),(8,7,'asdasd',1,0),(9,5,'fsdfsdf',1,0),(10,9,'sadsds',1,0),(11,5,'adsasda',1,0),(12,6,'werwerw',1,0),(13,9,'asdsafasfasdasd',1,0),(14,6,'safsfasfasfsafdsa',1,0),(15,7,'sad asdasda',1,0),(16,4,'raw rd ada wr awsada',1,0),(17,4,'asd asdaads',1,0),(18,5,'werw rwer wer w',1,0),(19,10,'sd fs fdfs',1,0),(20,9,'rwewe',1,0),(21,8,'ddfsd',1,0),(22,11,'sdgds',1,0),(23,5,' trr e',1,0),(24,13,'w erwe rwer wer',1,0),(25,6,'rtgsd',1,0),(26,7,'gtrfdw',1,0),(27,6,'sgrft',1,0),(28,2,'NetCracker',1,0),(29,7,'kaka',1,0);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-04 22:16:11
